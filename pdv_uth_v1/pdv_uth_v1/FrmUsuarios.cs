﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    //agregar referencias lib_pdv usuarios/personas
    public partial class FrmUsuarios : Form
    {
        int idCliente = 0;
        public FrmUsuarios()
        {
            InitializeComponent();
        }

        private void panelFormulario_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea cerrar la ventana de personas secundarias?",
                            "Cerrar",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void dgUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            idCliente = int.Parse(dgUsuarios.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgUsuarios.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApPat.Text = dgUsuarios.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApMat.Text = dgUsuarios.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtpFechaNacimiento.Value = DateTime.Parse(dgUsuarios.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCorreo.Text = dgUsuarios.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtCelular.Text = dgUsuarios.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtTelefono.Text = dgUsuarios.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtCalle.Text = dgUsuarios.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtNumCasa.Text = dgUsuarios.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtCP.Text = dgUsuarios.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtColonia.Text = dgUsuarios.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtFraccionamiento.Text = dgUsuarios.Rows[e.RowIndex].Cells[12].Value.ToString();
            txtLocalidad.Text = dgUsuarios.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtMunicipio.Text = dgUsuarios.Rows[e.RowIndex].Cells[14].Value.ToString();
            txtComprobanteDomicilio.Text = dgUsuarios.Rows[e.RowIndex].Cells[15].Value.ToString();
            txtComproINE.Text = dgUsuarios.Rows[e.RowIndex].Cells[16].Value.ToString();
            txtCurp.Text = dgUsuarios.Rows[e.RowIndex].Cells[17].Value.ToString();
            txtComprobanteCurp.Text = dgUsuarios.Rows[e.RowIndex].Cells[18].Value.ToString();
            txtActaDeNac.Text = dgUsuarios.Rows[e.RowIndex].Cells[19].Value.ToString();
            txtCertEstudios.Text = dgUsuarios.Rows[e.RowIndex].Cells[20].Value.ToString();
            cbTipoUser.Text = dgUsuarios.Rows[e.RowIndex].Cells[21].Value.ToString();
            txtUsuarioContra.Text = dgUsuarios.Rows[e.RowIndex].Cells[22].Value.ToString();
        }

        private void btnRetroceder_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmMenuPpal frm = new FrmMenuPpal();
            frm.Show();
        }
    }
}
