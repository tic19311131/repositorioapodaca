﻿namespace pdv_uth_v1
{
    partial class FrmCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCaja));
            this.dgListaProductos = new System.Windows.Forms.DataGridView();
            this.ColID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColSubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numericCantidad = new System.Windows.Forms.NumericUpDown();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.txtCodBarras = new System.Windows.Forms.TextBox();
            this.lblCodBarras = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnPagar = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panelImagenProducto = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.lblNombreProducto = new System.Windows.Forms.Label();
            this.pnlVentaInfo = new System.Windows.Forms.Panel();
            this.pnlCodBar = new System.Windows.Forms.Panel();
            this.picBoxCodBar = new System.Windows.Forms.PictureBox();
            this.btnLeerCodBar = new System.Windows.Forms.Button();
            this.lblCamara = new System.Windows.Forms.Label();
            this.cboCamara = new System.Windows.Forms.ComboBox();
            this.pnlTotales = new System.Windows.Forms.Panel();
            this.btnCancelarVenta = new System.Windows.Forms.Button();
            this.btnAgregarAVenta = new System.Windows.Forms.Button();
            this.txtIva = new System.Windows.Forms.TextBox();
            this.txtSubTotal = new System.Windows.Forms.TextBox();
            this.lblIva = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.pnlVentaBtns = new System.Windows.Forms.Panel();
            this.pnlNavbar = new System.Windows.Forms.Panel();
            this.panelTimer = new System.Windows.Forms.Panel();
            this.PnlInfo = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.picReloj = new System.Windows.Forms.PictureBox();
            this.lblReloj = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCajas = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCompras = new System.Windows.Forms.Button();
            this.btnCreditos = new System.Windows.Forms.Button();
            this.btnVentas = new System.Windows.Forms.Button();
            this.btnProductos = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.lblCajaTitle = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ipbLogo = new System.Windows.Forms.PictureBox();
            this.pnlBarraTareas = new System.Windows.Forms.Panel();
            this.txtConsultaProducto = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pnlMetodos = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelDatosCredito = new System.Windows.Forms.Panel();
            this.lblFechaProxPago = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblCreditoDisponible = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblClienteCredito = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnCredito = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtCredito = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnTarjeta = new System.Windows.Forms.Button();
            this.txtTarjeta = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnEfectivo = new System.Windows.Forms.Button();
            this.txtEfectivo = new System.Windows.Forms.TextBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgListaProductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCantidad)).BeginInit();
            this.panel5.SuspendLayout();
            this.panelImagenProducto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.pnlVentaInfo.SuspendLayout();
            this.pnlCodBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCodBar)).BeginInit();
            this.pnlTotales.SuspendLayout();
            this.pnlVentaBtns.SuspendLayout();
            this.pnlNavbar.SuspendLayout();
            this.PnlInfo.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picReloj)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipbLogo)).BeginInit();
            this.pnlBarraTareas.SuspendLayout();
            this.pnlMetodos.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelDatosCredito.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgListaProductos
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgListaProductos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgListaProductos.ColumnHeadersHeight = 25;
            this.dgListaProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColID,
            this.ColCodBarras,
            this.ColNombre,
            this.ColDescripcion,
            this.ColCantidad,
            this.ColPrecio,
            this.ColSubTotal});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgListaProductos.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgListaProductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListaProductos.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgListaProductos.Location = new System.Drawing.Point(0, 0);
            this.dgListaProductos.Margin = new System.Windows.Forms.Padding(2);
            this.dgListaProductos.Name = "dgListaProductos";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgListaProductos.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgListaProductos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgListaProductos.RowTemplate.Height = 24;
            this.dgListaProductos.Size = new System.Drawing.Size(802, 512);
            this.dgListaProductos.TabIndex = 0;
            // 
            // ColID
            // 
            this.ColID.Frozen = true;
            this.ColID.HeaderText = "ID";
            this.ColID.Name = "ColID";
            this.ColID.ReadOnly = true;
            this.ColID.Width = 43;
            // 
            // ColCodBarras
            // 
            this.ColCodBarras.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColCodBarras.Frozen = true;
            this.ColCodBarras.HeaderText = "Cod. Barras";
            this.ColCodBarras.MinimumWidth = 13;
            this.ColCodBarras.Name = "ColCodBarras";
            this.ColCodBarras.ReadOnly = true;
            this.ColCodBarras.Width = 87;
            // 
            // ColNombre
            // 
            this.ColNombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColNombre.HeaderText = "Producto";
            this.ColNombre.Name = "ColNombre";
            this.ColNombre.ReadOnly = true;
            this.ColNombre.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ColDescripcion
            // 
            this.ColDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColDescripcion.HeaderText = "Descripción";
            this.ColDescripcion.Name = "ColDescripcion";
            this.ColDescripcion.ReadOnly = true;
            this.ColDescripcion.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ColCantidad
            // 
            this.ColCantidad.HeaderText = "Cantidad";
            this.ColCantidad.Name = "ColCantidad";
            this.ColCantidad.ReadOnly = true;
            this.ColCantidad.Width = 70;
            // 
            // ColPrecio
            // 
            this.ColPrecio.HeaderText = "Precio";
            this.ColPrecio.Name = "ColPrecio";
            this.ColPrecio.ReadOnly = true;
            this.ColPrecio.Width = 62;
            // 
            // ColSubTotal
            // 
            this.ColSubTotal.HeaderText = "SubTotal";
            this.ColSubTotal.Name = "ColSubTotal";
            this.ColSubTotal.ReadOnly = true;
            this.ColSubTotal.Width = 75;
            // 
            // numericCantidad
            // 
            this.numericCantidad.DecimalPlaces = 3;
            this.numericCantidad.Font = new System.Drawing.Font("Trebuchet MS", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericCantidad.Location = new System.Drawing.Point(200, 67);
            this.numericCantidad.Margin = new System.Windows.Forms.Padding(2);
            this.numericCantidad.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericCantidad.Name = "numericCantidad";
            this.numericCantidad.Size = new System.Drawing.Size(84, 33);
            this.numericCantidad.TabIndex = 10;
            this.numericCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericCantidad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblCantidad
            // 
            this.lblCantidad.AutoSize = true;
            this.lblCantidad.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidad.ForeColor = System.Drawing.Color.Black;
            this.lblCantidad.Location = new System.Drawing.Point(207, 47);
            this.lblCantidad.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(78, 18);
            this.lblCantidad.TabIndex = 9;
            this.lblCantidad.Text = "Cantidad";
            // 
            // txtCodBarras
            // 
            this.txtCodBarras.Font = new System.Drawing.Font("Trebuchet MS", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodBarras.Location = new System.Drawing.Point(89, 232);
            this.txtCodBarras.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodBarras.Name = "txtCodBarras";
            this.txtCodBarras.Size = new System.Drawing.Size(186, 33);
            this.txtCodBarras.TabIndex = 8;
            this.txtCodBarras.Text = "1";
            // 
            // lblCodBarras
            // 
            this.lblCodBarras.AutoSize = true;
            this.lblCodBarras.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodBarras.ForeColor = System.Drawing.Color.Black;
            this.lblCodBarras.Location = new System.Drawing.Point(2, 229);
            this.lblCodBarras.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCodBarras.Name = "lblCodBarras";
            this.lblCodBarras.Size = new System.Drawing.Size(95, 36);
            this.lblCodBarras.TabIndex = 7;
            this.lblCodBarras.Text = "Código de \r\n    Barras";
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackColor = System.Drawing.Color.White;
            this.btnBuscar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Location = new System.Drawing.Point(0, 0);
            this.btnBuscar.Margin = new System.Windows.Forms.Padding(2);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(120, 46);
            this.btnBuscar.TabIndex = 7;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnPagar
            // 
            this.btnPagar.BackColor = System.Drawing.Color.Green;
            this.btnPagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPagar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnPagar.FlatAppearance.BorderSize = 2;
            this.btnPagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPagar.Font = new System.Drawing.Font("Lucida Bright", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPagar.ForeColor = System.Drawing.Color.Black;
            this.btnPagar.Image = ((System.Drawing.Image)(resources.GetObject("btnPagar.Image")));
            this.btnPagar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPagar.Location = new System.Drawing.Point(13, 7);
            this.btnPagar.Margin = new System.Windows.Forms.Padding(2);
            this.btnPagar.Name = "btnPagar";
            this.btnPagar.Size = new System.Drawing.Size(263, 61);
            this.btnPagar.TabIndex = 4;
            this.btnPagar.Text = "  Pagar";
            this.btnPagar.UseVisualStyleBackColor = false;
            this.btnPagar.Click += new System.EventHandler(this.btnPagar_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Gray;
            this.panel5.Controls.Add(this.panelImagenProducto);
            this.panel5.Controls.Add(this.pnlVentaInfo);
            this.panel5.Controls.Add(this.pnlVentaBtns);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Font = new System.Drawing.Font("Lucida Sans", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel5.Location = new System.Drawing.Point(1065, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(287, 749);
            this.panel5.TabIndex = 15;
            // 
            // panelImagenProducto
            // 
            this.panelImagenProducto.Controls.Add(this.pictureBox8);
            this.panelImagenProducto.Controls.Add(this.lblNombreProducto);
            this.panelImagenProducto.Controls.Add(this.numericCantidad);
            this.panelImagenProducto.Controls.Add(this.lblCantidad);
            this.panelImagenProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelImagenProducto.Location = new System.Drawing.Point(0, 0);
            this.panelImagenProducto.Name = "panelImagenProducto";
            this.panelImagenProducto.Size = new System.Drawing.Size(287, 184);
            this.panelImagenProducto.TabIndex = 16;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(3, 3);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(192, 159);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 0;
            this.pictureBox8.TabStop = false;
            // 
            // lblNombreProducto
            // 
            this.lblNombreProducto.AutoSize = true;
            this.lblNombreProducto.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreProducto.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblNombreProducto.Location = new System.Drawing.Point(28, 165);
            this.lblNombreProducto.Name = "lblNombreProducto";
            this.lblNombreProducto.Size = new System.Drawing.Size(129, 14);
            this.lblNombreProducto.TabIndex = 4;
            this.lblNombreProducto.Text = "NOMBRE  PRODUCTO";
            // 
            // pnlVentaInfo
            // 
            this.pnlVentaInfo.Controls.Add(this.pnlCodBar);
            this.pnlVentaInfo.Controls.Add(this.pnlTotales);
            this.pnlVentaInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlVentaInfo.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlVentaInfo.ForeColor = System.Drawing.Color.Black;
            this.pnlVentaInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlVentaInfo.Name = "pnlVentaInfo";
            this.pnlVentaInfo.Size = new System.Drawing.Size(287, 677);
            this.pnlVentaInfo.TabIndex = 16;
            // 
            // pnlCodBar
            // 
            this.pnlCodBar.BackColor = System.Drawing.Color.Gray;
            this.pnlCodBar.Controls.Add(this.txtCodBarras);
            this.pnlCodBar.Controls.Add(this.picBoxCodBar);
            this.pnlCodBar.Controls.Add(this.btnLeerCodBar);
            this.pnlCodBar.Controls.Add(this.lblCamara);
            this.pnlCodBar.Controls.Add(this.cboCamara);
            this.pnlCodBar.Controls.Add(this.lblCodBarras);
            this.pnlCodBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCodBar.Location = new System.Drawing.Point(0, 0);
            this.pnlCodBar.Name = "pnlCodBar";
            this.pnlCodBar.Size = new System.Drawing.Size(287, 491);
            this.pnlCodBar.TabIndex = 17;
            // 
            // picBoxCodBar
            // 
            this.picBoxCodBar.BackColor = System.Drawing.Color.Maroon;
            this.picBoxCodBar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picBoxCodBar.Location = new System.Drawing.Point(13, 270);
            this.picBoxCodBar.Name = "picBoxCodBar";
            this.picBoxCodBar.Size = new System.Drawing.Size(263, 181);
            this.picBoxCodBar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBoxCodBar.TabIndex = 15;
            this.picBoxCodBar.TabStop = false;
            // 
            // btnLeerCodBar
            // 
            this.btnLeerCodBar.Location = new System.Drawing.Point(200, 462);
            this.btnLeerCodBar.Name = "btnLeerCodBar";
            this.btnLeerCodBar.Size = new System.Drawing.Size(75, 23);
            this.btnLeerCodBar.TabIndex = 18;
            this.btnLeerCodBar.Text = "Leer";
            this.btnLeerCodBar.UseVisualStyleBackColor = true;
            // 
            // lblCamara
            // 
            this.lblCamara.AutoSize = true;
            this.lblCamara.Location = new System.Drawing.Point(6, 193);
            this.lblCamara.Name = "lblCamara";
            this.lblCamara.Size = new System.Drawing.Size(64, 18);
            this.lblCamara.TabIndex = 17;
            this.lblCamara.Text = "Camara";
            // 
            // cboCamara
            // 
            this.cboCamara.FormattingEnabled = true;
            this.cboCamara.Location = new System.Drawing.Point(79, 190);
            this.cboCamara.Name = "cboCamara";
            this.cboCamara.Size = new System.Drawing.Size(197, 26);
            this.cboCamara.TabIndex = 16;
            // 
            // pnlTotales
            // 
            this.pnlTotales.Controls.Add(this.btnCancelarVenta);
            this.pnlTotales.Controls.Add(this.btnAgregarAVenta);
            this.pnlTotales.Controls.Add(this.txtIva);
            this.pnlTotales.Controls.Add(this.txtSubTotal);
            this.pnlTotales.Controls.Add(this.lblIva);
            this.pnlTotales.Controls.Add(this.txtTotal);
            this.pnlTotales.Controls.Add(this.lblTotal);
            this.pnlTotales.Controls.Add(this.lblSubtotal);
            this.pnlTotales.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlTotales.Location = new System.Drawing.Point(0, 491);
            this.pnlTotales.Name = "pnlTotales";
            this.pnlTotales.Size = new System.Drawing.Size(287, 186);
            this.pnlTotales.TabIndex = 16;
            // 
            // btnCancelarVenta
            // 
            this.btnCancelarVenta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnCancelarVenta.FlatAppearance.BorderSize = 2;
            this.btnCancelarVenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarVenta.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelarVenta.ForeColor = System.Drawing.Color.Black;
            this.btnCancelarVenta.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelarVenta.Image")));
            this.btnCancelarVenta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelarVenta.Location = new System.Drawing.Point(152, 14);
            this.btnCancelarVenta.Name = "btnCancelarVenta";
            this.btnCancelarVenta.Size = new System.Drawing.Size(124, 57);
            this.btnCancelarVenta.TabIndex = 15;
            this.btnCancelarVenta.Text = "Cancelar ";
            this.btnCancelarVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelarVenta.UseVisualStyleBackColor = false;
            // 
            // btnAgregarAVenta
            // 
            this.btnAgregarAVenta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnAgregarAVenta.FlatAppearance.BorderSize = 2;
            this.btnAgregarAVenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarAVenta.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarAVenta.ForeColor = System.Drawing.Color.Black;
            this.btnAgregarAVenta.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregarAVenta.Image")));
            this.btnAgregarAVenta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarAVenta.Location = new System.Drawing.Point(13, 14);
            this.btnAgregarAVenta.Name = "btnAgregarAVenta";
            this.btnAgregarAVenta.Size = new System.Drawing.Size(124, 57);
            this.btnAgregarAVenta.TabIndex = 14;
            this.btnAgregarAVenta.Text = "Agregar";
            this.btnAgregarAVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAgregarAVenta.UseVisualStyleBackColor = false;
            this.btnAgregarAVenta.Click += new System.EventHandler(this.btnAgregarAVenta_Click);
            // 
            // txtIva
            // 
            this.txtIva.Font = new System.Drawing.Font("Trebuchet MS", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIva.Location = new System.Drawing.Point(114, 113);
            this.txtIva.Margin = new System.Windows.Forms.Padding(2);
            this.txtIva.Name = "txtIva";
            this.txtIva.Size = new System.Drawing.Size(147, 33);
            this.txtIva.TabIndex = 18;
            this.txtIva.Text = "0";
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.Font = new System.Drawing.Font("Trebuchet MS", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.Location = new System.Drawing.Point(114, 76);
            this.txtSubTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.Size = new System.Drawing.Size(147, 33);
            this.txtSubTotal.TabIndex = 16;
            this.txtSubTotal.Text = "0";
            // 
            // lblIva
            // 
            this.lblIva.AutoSize = true;
            this.lblIva.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIva.ForeColor = System.Drawing.Color.Black;
            this.lblIva.Location = new System.Drawing.Point(41, 122);
            this.lblIva.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIva.Name = "lblIva";
            this.lblIva.Size = new System.Drawing.Size(35, 18);
            this.lblIva.TabIndex = 17;
            this.lblIva.Text = "IVA";
            // 
            // txtTotal
            // 
            this.txtTotal.Font = new System.Drawing.Font("Trebuchet MS", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(114, 150);
            this.txtTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(147, 33);
            this.txtTotal.TabIndex = 20;
            this.txtTotal.Text = "0";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.Black;
            this.lblTotal.Location = new System.Drawing.Point(30, 159);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(58, 18);
            this.lblTotal.TabIndex = 19;
            this.lblTotal.Text = "TOTAL";
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.AutoSize = true;
            this.lblSubtotal.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtotal.ForeColor = System.Drawing.Color.Black;
            this.lblSubtotal.Location = new System.Drawing.Point(18, 85);
            this.lblSubtotal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(70, 18);
            this.lblSubtotal.TabIndex = 15;
            this.lblSubtotal.Text = "Subtotal";
            // 
            // pnlVentaBtns
            // 
            this.pnlVentaBtns.Controls.Add(this.btnPagar);
            this.pnlVentaBtns.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlVentaBtns.Location = new System.Drawing.Point(0, 677);
            this.pnlVentaBtns.Name = "pnlVentaBtns";
            this.pnlVentaBtns.Size = new System.Drawing.Size(287, 72);
            this.pnlVentaBtns.TabIndex = 17;
            // 
            // pnlNavbar
            // 
            this.pnlNavbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.pnlNavbar.Controls.Add(this.panelTimer);
            this.pnlNavbar.Controls.Add(this.PnlInfo);
            this.pnlNavbar.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlNavbar.Location = new System.Drawing.Point(0, 0);
            this.pnlNavbar.Name = "pnlNavbar";
            this.pnlNavbar.Size = new System.Drawing.Size(263, 749);
            this.pnlNavbar.TabIndex = 17;
            // 
            // panelTimer
            // 
            this.panelTimer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTimer.Location = new System.Drawing.Point(269, 623);
            this.panelTimer.Name = "panelTimer";
            this.panelTimer.Size = new System.Drawing.Size(0, 126);
            this.panelTimer.TabIndex = 43;
            // 
            // PnlInfo
            // 
            this.PnlInfo.BackColor = System.Drawing.Color.Gray;
            this.PnlInfo.Controls.Add(this.panel7);
            this.PnlInfo.Controls.Add(this.panel4);
            this.PnlInfo.Controls.Add(this.panel3);
            this.PnlInfo.Controls.Add(this.pictureBox14);
            this.PnlInfo.Controls.Add(this.lblCajaTitle);
            this.PnlInfo.Controls.Add(this.label15);
            this.PnlInfo.Controls.Add(this.ipbLogo);
            this.PnlInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.PnlInfo.Location = new System.Drawing.Point(0, 0);
            this.PnlInfo.Name = "PnlInfo";
            this.PnlInfo.Size = new System.Drawing.Size(269, 749);
            this.PnlInfo.TabIndex = 41;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Gray;
            this.panel7.Controls.Add(this.picReloj);
            this.panel7.Controls.Add(this.lblReloj);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 689);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(269, 60);
            this.panel7.TabIndex = 49;
            // 
            // picReloj
            // 
            this.picReloj.Image = ((System.Drawing.Image)(resources.GetObject("picReloj.Image")));
            this.picReloj.Location = new System.Drawing.Point(0, 0);
            this.picReloj.Name = "picReloj";
            this.picReloj.Size = new System.Drawing.Size(64, 58);
            this.picReloj.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picReloj.TabIndex = 1;
            this.picReloj.TabStop = false;
            // 
            // lblReloj
            // 
            this.lblReloj.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReloj.ForeColor = System.Drawing.Color.Black;
            this.lblReloj.Location = new System.Drawing.Point(70, 18);
            this.lblReloj.Name = "lblReloj";
            this.lblReloj.Size = new System.Drawing.Size(183, 32);
            this.lblReloj.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Gray;
            this.panel4.Controls.Add(this.btnCajas);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.btnCompras);
            this.panel4.Controls.Add(this.btnCreditos);
            this.panel4.Controls.Add(this.btnVentas);
            this.panel4.Controls.Add(this.btnProductos);
            this.panel4.Controls.Add(this.btnHome);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Location = new System.Drawing.Point(0, 253);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(263, 496);
            this.panel4.TabIndex = 42;
            // 
            // btnCajas
            // 
            this.btnCajas.BackColor = System.Drawing.Color.Gray;
            this.btnCajas.FlatAppearance.BorderSize = 2;
            this.btnCajas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCajas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnCajas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCajas.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCajas.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCajas.Image = ((System.Drawing.Image)(resources.GetObject("btnCajas.Image")));
            this.btnCajas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCajas.Location = new System.Drawing.Point(6, 279);
            this.btnCajas.Name = "btnCajas";
            this.btnCajas.Size = new System.Drawing.Size(249, 47);
            this.btnCajas.TabIndex = 48;
            this.btnCajas.Text = "   Cajas";
            this.btnCajas.UseVisualStyleBackColor = false;
            this.btnCajas.Click += new System.EventHandler(this.btnCajas_Click);
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(3, 326);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(250, 3);
            this.label2.TabIndex = 47;
            this.label2.Text = "label2";
            this.label2.UseWaitCursor = true;
            // 
            // btnCompras
            // 
            this.btnCompras.BackColor = System.Drawing.Color.Gray;
            this.btnCompras.FlatAppearance.BorderSize = 2;
            this.btnCompras.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCompras.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnCompras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCompras.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompras.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCompras.Image = ((System.Drawing.Image)(resources.GetObject("btnCompras.Image")));
            this.btnCompras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCompras.Location = new System.Drawing.Point(6, 226);
            this.btnCompras.Name = "btnCompras";
            this.btnCompras.Size = new System.Drawing.Size(249, 47);
            this.btnCompras.TabIndex = 44;
            this.btnCompras.Text = "   Compras";
            this.btnCompras.UseVisualStyleBackColor = false;
            this.btnCompras.Click += new System.EventHandler(this.btnCompras_Click);
            // 
            // btnCreditos
            // 
            this.btnCreditos.BackColor = System.Drawing.Color.Gray;
            this.btnCreditos.FlatAppearance.BorderSize = 2;
            this.btnCreditos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCreditos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnCreditos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreditos.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreditos.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCreditos.Image = ((System.Drawing.Image)(resources.GetObject("btnCreditos.Image")));
            this.btnCreditos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreditos.Location = new System.Drawing.Point(6, 173);
            this.btnCreditos.Name = "btnCreditos";
            this.btnCreditos.Size = new System.Drawing.Size(249, 47);
            this.btnCreditos.TabIndex = 43;
            this.btnCreditos.Text = "    Creditos";
            this.btnCreditos.UseVisualStyleBackColor = false;
            this.btnCreditos.Click += new System.EventHandler(this.btnCreditos_Click);
            // 
            // btnVentas
            // 
            this.btnVentas.BackColor = System.Drawing.Color.Gray;
            this.btnVentas.FlatAppearance.BorderSize = 2;
            this.btnVentas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnVentas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnVentas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVentas.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVentas.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnVentas.Image = ((System.Drawing.Image)(resources.GetObject("btnVentas.Image")));
            this.btnVentas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVentas.Location = new System.Drawing.Point(6, 61);
            this.btnVentas.Name = "btnVentas";
            this.btnVentas.Size = new System.Drawing.Size(249, 47);
            this.btnVentas.TabIndex = 42;
            this.btnVentas.Text = "    Ventas";
            this.btnVentas.UseVisualStyleBackColor = false;
            this.btnVentas.Click += new System.EventHandler(this.btnVentas_Click);
            // 
            // btnProductos
            // 
            this.btnProductos.BackColor = System.Drawing.Color.Gray;
            this.btnProductos.FlatAppearance.BorderSize = 2;
            this.btnProductos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnProductos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductos.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProductos.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnProductos.Image = ((System.Drawing.Image)(resources.GetObject("btnProductos.Image")));
            this.btnProductos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProductos.Location = new System.Drawing.Point(6, 120);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Size = new System.Drawing.Size(249, 47);
            this.btnProductos.TabIndex = 41;
            this.btnProductos.Text = "     Productos";
            this.btnProductos.UseVisualStyleBackColor = false;
            this.btnProductos.Click += new System.EventHandler(this.btnProductos_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.Gray;
            this.btnHome.FlatAppearance.BorderSize = 2;
            this.btnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.Location = new System.Drawing.Point(6, 5);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(249, 47);
            this.btnHome.TabIndex = 40;
            this.btnHome.Text = "     Inicio";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // label17
            // 
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Location = new System.Drawing.Point(5, 110);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(250, 3);
            this.label17.TabIndex = 39;
            this.label17.Text = "label17";
            this.label17.UseWaitCursor = true;
            // 
            // label18
            // 
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Location = new System.Drawing.Point(6, 52);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(250, 3);
            this.label18.TabIndex = 38;
            this.label18.Text = "label18";
            this.label18.UseWaitCursor = true;
            // 
            // label20
            // 
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label20.Location = new System.Drawing.Point(6, 221);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(250, 3);
            this.label20.TabIndex = 37;
            this.label20.Text = "label20";
            this.label20.UseWaitCursor = true;
            // 
            // label21
            // 
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Location = new System.Drawing.Point(6, 273);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(250, 3);
            this.label21.TabIndex = 36;
            this.label21.Text = "label21";
            this.label21.UseWaitCursor = true;
            // 
            // label22
            // 
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.Location = new System.Drawing.Point(6, 168);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(250, 3);
            this.label22.TabIndex = 35;
            this.label22.Text = "label22";
            this.label22.UseWaitCursor = true;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(268, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(600, 42);
            this.panel3.TabIndex = 48;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.Gray;
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox14.InitialImage")));
            this.pictureBox14.Location = new System.Drawing.Point(10, 1);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(54, 58);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox14.TabIndex = 47;
            this.pictureBox14.TabStop = false;
            // 
            // lblCajaTitle
            // 
            this.lblCajaTitle.AutoSize = true;
            this.lblCajaTitle.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCajaTitle.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblCajaTitle.Location = new System.Drawing.Point(93, 12);
            this.lblCajaTitle.Name = "lblCajaTitle";
            this.lblCajaTitle.Size = new System.Drawing.Size(77, 33);
            this.lblCajaTitle.TabIndex = 46;
            this.lblCajaTitle.Text = "Caja";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(68, 236);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(133, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "NOMBRE DEL USUARIO ";
            this.label15.UseWaitCursor = true;
            // 
            // ipbLogo
            // 
            this.ipbLogo.Image = ((System.Drawing.Image)(resources.GetObject("ipbLogo.Image")));
            this.ipbLogo.Location = new System.Drawing.Point(45, 67);
            this.ipbLogo.Name = "ipbLogo";
            this.ipbLogo.Size = new System.Drawing.Size(175, 168);
            this.ipbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ipbLogo.TabIndex = 43;
            this.ipbLogo.TabStop = false;
            this.ipbLogo.UseWaitCursor = true;
            // 
            // pnlBarraTareas
            // 
            this.pnlBarraTareas.BackColor = System.Drawing.Color.Gray;
            this.pnlBarraTareas.Controls.Add(this.panel13);
            this.pnlBarraTareas.Controls.Add(this.txtConsultaProducto);
            this.pnlBarraTareas.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBarraTareas.Location = new System.Drawing.Point(263, 0);
            this.pnlBarraTareas.Name = "pnlBarraTareas";
            this.pnlBarraTareas.Size = new System.Drawing.Size(802, 67);
            this.pnlBarraTareas.TabIndex = 18;
            // 
            // txtConsultaProducto
            // 
            this.txtConsultaProducto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConsultaProducto.Location = new System.Drawing.Point(110, 25);
            this.txtConsultaProducto.Name = "txtConsultaProducto";
            this.txtConsultaProducto.Size = new System.Drawing.Size(321, 20);
            this.txtConsultaProducto.TabIndex = 41;
            // 
            // panel6
            // 
            this.panel6.Location = new System.Drawing.Point(263, 67);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(802, 320);
            this.panel6.TabIndex = 19;
            // 
            // pnlMetodos
            // 
            this.pnlMetodos.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnlMetodos.Controls.Add(this.panel1);
            this.pnlMetodos.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlMetodos.Location = new System.Drawing.Point(263, 579);
            this.pnlMetodos.Name = "pnlMetodos";
            this.pnlMetodos.Size = new System.Drawing.Size(802, 170);
            this.pnlMetodos.TabIndex = 20;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Controls.Add(this.panelDatosCredito);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(802, 170);
            this.panel1.TabIndex = 18;
            // 
            // panelDatosCredito
            // 
            this.panelDatosCredito.BackColor = System.Drawing.Color.Gray;
            this.panelDatosCredito.Controls.Add(this.lblFechaProxPago);
            this.panelDatosCredito.Controls.Add(this.label8);
            this.panelDatosCredito.Controls.Add(this.lblCreditoDisponible);
            this.panelDatosCredito.Controls.Add(this.label6);
            this.panelDatosCredito.Controls.Add(this.lblClienteCredito);
            this.panelDatosCredito.Controls.Add(this.label3);
            this.panelDatosCredito.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDatosCredito.Location = new System.Drawing.Point(305, 0);
            this.panelDatosCredito.Margin = new System.Windows.Forms.Padding(2);
            this.panelDatosCredito.Name = "panelDatosCredito";
            this.panelDatosCredito.Size = new System.Drawing.Size(347, 170);
            this.panelDatosCredito.TabIndex = 19;
            this.panelDatosCredito.Visible = false;
            // 
            // lblFechaProxPago
            // 
            this.lblFechaProxPago.AutoSize = true;
            this.lblFechaProxPago.BackColor = System.Drawing.Color.Gray;
            this.lblFechaProxPago.Font = new System.Drawing.Font("Lucida Sans", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaProxPago.ForeColor = System.Drawing.Color.Black;
            this.lblFechaProxPago.Location = new System.Drawing.Point(128, 99);
            this.lblFechaProxPago.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFechaProxPago.Name = "lblFechaProxPago";
            this.lblFechaProxPago.Size = new System.Drawing.Size(136, 23);
            this.lblFechaProxPago.TabIndex = 5;
            this.lblFechaProxPago.Text = "30/02/2021";
            this.lblFechaProxPago.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Gray;
            this.label8.Font = new System.Drawing.Font("Lucida Sans", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(2, 98);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 22);
            this.label8.TabIndex = 4;
            this.label8.Text = "Prox. Pago:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblCreditoDisponible
            // 
            this.lblCreditoDisponible.AutoSize = true;
            this.lblCreditoDisponible.BackColor = System.Drawing.Color.Gray;
            this.lblCreditoDisponible.Font = new System.Drawing.Font("Lucida Sans", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditoDisponible.ForeColor = System.Drawing.Color.Black;
            this.lblCreditoDisponible.Location = new System.Drawing.Point(128, 58);
            this.lblCreditoDisponible.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCreditoDisponible.Name = "lblCreditoDisponible";
            this.lblCreditoDisponible.Size = new System.Drawing.Size(94, 23);
            this.lblCreditoDisponible.TabIndex = 3;
            this.lblCreditoDisponible.Text = "$999.00";
            this.lblCreditoDisponible.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Gray;
            this.label6.Font = new System.Drawing.Font("Lucida Sans", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(4, 57);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 22);
            this.label6.TabIndex = 2;
            this.label6.Text = "Disponible:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblClienteCredito
            // 
            this.lblClienteCredito.AutoSize = true;
            this.lblClienteCredito.BackColor = System.Drawing.Color.Gray;
            this.lblClienteCredito.Font = new System.Drawing.Font("Lucida Sans", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClienteCredito.ForeColor = System.Drawing.Color.Black;
            this.lblClienteCredito.Location = new System.Drawing.Point(99, 13);
            this.lblClienteCredito.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblClienteCredito.Name = "lblClienteCredito";
            this.lblClienteCredito.Size = new System.Drawing.Size(93, 23);
            this.lblClienteCredito.TabIndex = 1;
            this.lblClienteCredito.Text = "Foraneo";
            this.lblClienteCredito.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Gray;
            this.label3.Font = new System.Drawing.Font("Lucida Sans", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(4, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 22);
            this.label3.TabIndex = 0;
            this.label3.Text = "Cliente:";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.button1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(652, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(150, 170);
            this.panel8.TabIndex = 18;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Trebuchet MS", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(14, 15);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 146);
            this.button1.TabIndex = 5;
            this.button1.Text = "  Cliente";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel11);
            this.panel2.Controls.Add(this.panel10);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(305, 170);
            this.panel2.TabIndex = 17;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btnCredito);
            this.panel11.Controls.Add(this.pictureBox2);
            this.panel11.Controls.Add(this.txtCredito);
            this.panel11.Location = new System.Drawing.Point(205, 6);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(89, 164);
            this.panel11.TabIndex = 4;
            // 
            // btnCredito
            // 
            this.btnCredito.BackColor = System.Drawing.Color.Gray;
            this.btnCredito.Image = ((System.Drawing.Image)(resources.GetObject("btnCredito.Image")));
            this.btnCredito.Location = new System.Drawing.Point(0, 31);
            this.btnCredito.Name = "btnCredito";
            this.btnCredito.Size = new System.Drawing.Size(84, 78);
            this.btnCredito.TabIndex = 2;
            this.btnCredito.UseVisualStyleBackColor = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.ImageLocation = "";
            this.pictureBox2.Location = new System.Drawing.Point(5, 31);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(79, 78);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // txtCredito
            // 
            this.txtCredito.Font = new System.Drawing.Font("Trebuchet MS", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCredito.Location = new System.Drawing.Point(2, 133);
            this.txtCredito.Margin = new System.Windows.Forms.Padding(2);
            this.txtCredito.Name = "txtCredito";
            this.txtCredito.Size = new System.Drawing.Size(82, 29);
            this.txtCredito.TabIndex = 1;
            this.txtCredito.Text = "0.00";
            this.txtCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCredito.Leave += new System.EventHandler(this.txtCredito_Leave);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btnTarjeta);
            this.panel10.Controls.Add(this.txtTarjeta);
            this.panel10.Location = new System.Drawing.Point(110, 6);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(95, 164);
            this.panel10.TabIndex = 3;
            // 
            // btnTarjeta
            // 
            this.btnTarjeta.BackColor = System.Drawing.Color.Gray;
            this.btnTarjeta.Image = ((System.Drawing.Image)(resources.GetObject("btnTarjeta.Image")));
            this.btnTarjeta.Location = new System.Drawing.Point(5, 31);
            this.btnTarjeta.Name = "btnTarjeta";
            this.btnTarjeta.Size = new System.Drawing.Size(84, 78);
            this.btnTarjeta.TabIndex = 3;
            this.btnTarjeta.UseVisualStyleBackColor = false;
            // 
            // txtTarjeta
            // 
            this.txtTarjeta.Font = new System.Drawing.Font("Trebuchet MS", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTarjeta.Location = new System.Drawing.Point(2, 133);
            this.txtTarjeta.Margin = new System.Windows.Forms.Padding(2);
            this.txtTarjeta.Name = "txtTarjeta";
            this.txtTarjeta.Size = new System.Drawing.Size(82, 29);
            this.txtTarjeta.TabIndex = 1;
            this.txtTarjeta.Text = "0.00";
            this.txtTarjeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTarjeta.Leave += new System.EventHandler(this.txtTarjeta_Leave);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.btnEfectivo);
            this.panel9.Controls.Add(this.txtEfectivo);
            this.panel9.Location = new System.Drawing.Point(12, 6);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(98, 162);
            this.panel9.TabIndex = 2;
            // 
            // btnEfectivo
            // 
            this.btnEfectivo.BackColor = System.Drawing.Color.Gray;
            this.btnEfectivo.Image = ((System.Drawing.Image)(resources.GetObject("btnEfectivo.Image")));
            this.btnEfectivo.Location = new System.Drawing.Point(8, 29);
            this.btnEfectivo.Name = "btnEfectivo";
            this.btnEfectivo.Size = new System.Drawing.Size(84, 78);
            this.btnEfectivo.TabIndex = 3;
            this.btnEfectivo.UseVisualStyleBackColor = false;
            // 
            // txtEfectivo
            // 
            this.txtEfectivo.Font = new System.Drawing.Font("Trebuchet MS", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEfectivo.Location = new System.Drawing.Point(6, 132);
            this.txtEfectivo.Margin = new System.Windows.Forms.Padding(2);
            this.txtEfectivo.Name = "txtEfectivo";
            this.txtEfectivo.Size = new System.Drawing.Size(82, 29);
            this.txtEfectivo.TabIndex = 1;
            this.txtEfectivo.Text = "0.00";
            this.txtEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtEfectivo.Leave += new System.EventHandler(this.txtEfectivo_Leave);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel12.Controls.Add(this.dgListaProductos);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(263, 67);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(802, 512);
            this.panel12.TabIndex = 21;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.btnBuscar);
            this.panel13.Location = new System.Drawing.Point(460, 12);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(120, 46);
            this.panel13.TabIndex = 42;
            // 
            // FrmCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 749);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.pnlMetodos);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pnlBarraTareas);
            this.Controls.Add(this.pnlNavbar);
            this.Controls.Add(this.panel5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmCaja";
            this.Text = "FrmCaja";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dgListaProductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCantidad)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panelImagenProducto.ResumeLayout(false);
            this.panelImagenProducto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.pnlVentaInfo.ResumeLayout(false);
            this.pnlCodBar.ResumeLayout(false);
            this.pnlCodBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCodBar)).EndInit();
            this.pnlTotales.ResumeLayout(false);
            this.pnlTotales.PerformLayout();
            this.pnlVentaBtns.ResumeLayout(false);
            this.pnlNavbar.ResumeLayout(false);
            this.PnlInfo.ResumeLayout(false);
            this.PnlInfo.PerformLayout();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picReloj)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipbLogo)).EndInit();
            this.pnlBarraTareas.ResumeLayout(false);
            this.pnlBarraTareas.PerformLayout();
            this.pnlMetodos.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panelDatosCredito.ResumeLayout(false);
            this.panelDatosCredito.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.NumericUpDown numericCantidad;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.TextBox txtCodBarras;
        private System.Windows.Forms.Label lblCodBarras;
        private System.Windows.Forms.DataGridView dgListaProductos;
        private System.Windows.Forms.Button btnPagar;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnCancelarVenta;
        private System.Windows.Forms.Button btnAgregarAVenta;
        private System.Windows.Forms.Label lblNombreProducto;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel pnlVentaBtns;
        private System.Windows.Forms.Panel pnlVentaInfo;
        private System.Windows.Forms.Panel pnlNavbar;
        private System.Windows.Forms.Panel panelTimer;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnCompras;
        private System.Windows.Forms.Button btnCreditos;
        private System.Windows.Forms.Button btnVentas;
        private System.Windows.Forms.Button btnProductos;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel PnlInfo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Label lblCajaTitle;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox ipbLogo;
        private System.Windows.Forms.Panel pnlBarraTareas;
        private System.Windows.Forms.TextBox txtConsultaProducto;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox picReloj;
        private System.Windows.Forms.Label lblReloj;
        private System.Windows.Forms.Button btnCajas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelImagenProducto;
        private System.Windows.Forms.PictureBox picBoxCodBar;
        private System.Windows.Forms.Panel pnlMetodos;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelDatosCredito;
        private System.Windows.Forms.Label lblFechaProxPago;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblCreditoDisponible;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblClienteCredito;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtCredito;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TextBox txtTarjeta;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox txtEfectivo;
        private System.Windows.Forms.Panel pnlCodBar;
        private System.Windows.Forms.Panel pnlTotales;
        private System.Windows.Forms.TextBox txtIva;
        private System.Windows.Forms.TextBox txtSubTotal;
        private System.Windows.Forms.Label lblIva;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblSubtotal;
        private System.Windows.Forms.Label lblCamara;
        private System.Windows.Forms.ComboBox cboCamara;
        private System.Windows.Forms.Button btnLeerCodBar;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btnCredito;
        private System.Windows.Forms.Button btnTarjeta;
        private System.Windows.Forms.Button btnEfectivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPrecio;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSubTotal;
        private System.Windows.Forms.Panel panel13;
    }
}