﻿using AForge.Video;
using AForge.Video.DirectShow;
using Lib_pdv_uth_v1.cajas;
using Lib_pdv_uth_v1.productos;
using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using ZXing;

namespace pdv_uth_v1
{
    public partial class FrmCaja : Form
    {
        private Timer ti;

        static public double totalM;

        private void eventTimer(object ob, EventArgs e)
        {
            DateTime today = DateTime.Now;
            lblReloj.Text = today.ToString("hh:mm:ss tt");

        }
        Caja caja = new Caja();
        public FrmCaja()
        {
            ti = new Timer();
            ti.Tick += new EventHandler(eventTimer);
            ti.Enabled = true;

            InitializeComponent();
        }

        //   Poniendo el lector de Codigo de Barras
        FilterInfoCollection filterInfoCollection;
        VideoCaptureDevice videoCaptureDevice;

        //Configurando ComboBox para seleccionar la camara que se desea utilizar
        private void FrmCaja_Load(object sender, EventArgs e)
        {
            filterInfoCollection = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo device in filterInfoCollection)
                cboCamara.Items.Add(device.Name);
            cboCamara.SelectedIndex = 0;
        }
        //Boton para iniciar la camara para leer el codigo de barras
        private void btnLeerCodBar_Click(object sender, EventArgs e)
        {
            videoCaptureDevice = new VideoCaptureDevice(filterInfoCollection[cboCamara.SelectedIndex].MonikerString);
            videoCaptureDevice.NewFrame += capturaDeVideo;
            videoCaptureDevice.Start();
        }
        //Captura del codigo de barras y transformarlo a string
        private void capturaDeVideo(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap bitmap = (Bitmap)eventArgs.Frame.Clone();
            BarcodeReader reader = new BarcodeReader();
            var result = reader.Decode(bitmap);
            if (result != null)
            {
                txtCodBarras.Invoke(new MethodInvoker(delegate ()
                {
                    txtCodBarras.Text = result.ToString();
                }));
            }

            picBoxCodBar.Image = bitmap;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            panelDatosCredito.Visible = true;

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea cerrar la caja?",
                                "Salir",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }

        }








        // public Caja consultarMacAdd(string mac)
        // {
        //     Caja resultado;
        //     object[] arreglo = db.consultarUnRegistro("*", "cajas", "mac-adress-'" + mac + "'");
        //     if (arreglo!= null)
        //     {
        //         //mapeo de arreglo al objeto caja
        //         resultado=new Caja
        //         {
        //             Id = int.Parse(arreglo.ToString)
        //         }
        //     }
        //
        // }



        //  public void formatEfectivo (TextBox textBox)
        //  {
        //      if (txtEfectivo.Text == string.Empty)
        //      { 
        //          txtEfectivo.Text = "0.00";
        //          double.Parse(txtEfectivo.Text);
        //      }
        //      else
        //      {
        //          return;
        //      }
        //  }
        //
        //  public void formatCredito(TextBox textBox)
        //  {
        //      if (txtEfectivo.Text == string.Empty)
        //      {
        //          return;
        //      }
        //      else
        //      {
        //          txtCredito.Text = "0.00";
        //          double.Parse(txtCredito.Text);
        //
        //      }
        //  }
        //  public void formatTarjeta(TextBox textBox)
        //  {
        //      if (txtEfectivo.Text == string.Empty)
        //      {
        //          return;
        //      }
        //      else
        //      {
        //          txtTarjeta.Text = "0.00";
        //          double.Parse(txtTarjeta.Text);
        //
        //      }
        //  }



        private void btnPagar_Click(object sender, EventArgs e)
        {
            double efectivoM = double.Parse(txtEfectivo.Text);
            double tarjetaM = double.Parse(txtTarjeta.Text);
            double creditoM = double.Parse(txtCredito.Text);
            totalM = double.Parse(txtTotal.Text);
            double totalTotal = efectivoM + tarjetaM + creditoM;

            if (double.Parse(txtEfectivo.Text) + double.Parse(txtTarjeta.Text) + double.Parse(txtCredito.Text) >= double.Parse(txtTotal.Text))
            // if(double.Parse(txtCredito.Text)<= 0 && double.Parse(txtTarjeta.Text) <0) 

            {   //1
                if (efectivoM >= totalM)// && tarjetaM==0 && creditoM==0)
                {
                    MessageBox.Show("Pago solo efectivo");
                    caja.vender(FrmLogin.us.Id, totalTotal, totalM);
                }
                //2
                else if (tarjetaM >= totalM)// && efectivoM == 0 && creditoM==0)
                {
                    MessageBox.Show("Pago solo tarjeta");
                }
                //3
                else if (creditoM >= totalM)//&& efectivoM==0 && tarjetaM==0)
                {
                    MessageBox.Show("Pago solo Credito");
                }
                //4
                else if (efectivoM + tarjetaM >= totalM)
                {
                    MessageBox.Show("Pago tarjeta y efectivo");
                }
                //5
                else if (efectivoM + creditoM >= totalM)
                {
                    MessageBox.Show("Pago Efectivo y Credito");
                }
                //6
                else if (tarjetaM + creditoM >= totalM)
                {
                    MessageBox.Show("Pago tarjeta y credito");
                }
                //7
                else if (totalTotal >= 0)
                {
                    caja.vender(FrmLogin.us.Id, totalTotal, totalM);
                    MessageBox.Show("Pago con todos");
                }

            }
            //venta con metodo de pago efectivo
            else
                MessageBox.Show("Error en el registro de la venta. " + Caja.msgError);
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            //buscar metodo
        }

        private void btnAgregarAVenta_Click(object sender, EventArgs e)
        {
            Producto prod = new Producto();
            prod = prod.consultarPorCodigoDeBarras(txtCodBarras.Text);

            
            //agregamos un renglon con la info del producto CAPTURADo al DG
            if (prod != null)
            {
                //pasar producto obj a obj[]
                //agregar el producto al DataGrid
                dgListaProductos.Rows.Add(new object[] { prod.Id, prod.CodigoDeBarras, prod.Nombre, prod.Descripcion, numericCantidad.Value, prod.Precio, (prod.Precio * double.Parse(numericCantidad.Value.ToString())) });
                //agregar el produ a caja.ListaProductos
                caja.ListaProductos.Add(new ProductosAVender(prod.Id, double.Parse(numericCantidad.Value.ToString()), prod.CodigoDeBarras));
                //limpiamos los text
                txtCodBarras.Clear();
                numericCantidad.Value = 1;
            }
            else
            {
                //producto NO EXISTE
                MessageBox.Show("El producto con el Código de Barras <" + txtCodBarras.Text + ">, no Existe. ", "No encontrado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //cursor a CodBarras y select all
                txtCodBarras.Focus();
                txtCodBarras.SelectAll();
            }
            double temp = 0;
            for (int i = 0; i < dgListaProductos.RowCount - 1; i++)
            {
                temp = temp +
                    double.Parse(dgListaProductos.Rows[i].Cells[dgListaProductos.ColumnCount - 1].Value.ToString());
            }
            txtEfectivo.Text = double.Parse(temp.ToString()).ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));
            txtTotal.Text = double.Parse(temp.ToString()).ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));
            txtSubTotal.Text = double.Parse((temp * 0.84).ToString()).ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));
            txtIva.Text = double.Parse((temp * 0.16).ToString()).ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));
        }
        //txtPrecio.Text =double.Parse(dgProductos.Rows[e.RowIndex].Cells[3].Value.ToString()).ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));


        private void btnHome_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmMenuPpal frm = new FrmMenuPpal();
            frm.Show();
        }
        private void btnVentas_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmVentas frm = new FrmVentas();
            frm.Show();
        }
        private void btnProductos_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCatalogoProductos frm = new FrmCatalogoProductos();
            frm.Show();
        }
        private void btnCreditos_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCreditos frm = new FrmCreditos();
            frm.Show();

        }
        private void btnCompras_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCompras frm = new FrmCompras();
            frm.Show();
        }

        private void btnCajas_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCaja frm = new FrmCaja();
            frm.Show();

        }
        private void btnExpandirMn_Click(object sender, EventArgs e)
        {
            if (pnlNavbar.Width == 269)
            {
                //Quitar bordes al contraer menu
                pnlNavbar.Width = 69;
                btnHome.FlatAppearance.BorderSize = 0;
                btnCompras.FlatAppearance.BorderSize = 0;
                btnProductos.FlatAppearance.BorderSize = 0;
                btnCreditos.FlatAppearance.BorderSize = 0;
                btnVentas.FlatAppearance.BorderSize = 0;
                ipbLogo.Visible = false;
            }
            //activar bordes al expandir menu
            else
            {
                pnlNavbar.Width = 269;
                btnHome.FlatAppearance.BorderSize = 2;
                btnCompras.FlatAppearance.BorderSize = 2;
                btnProductos.FlatAppearance.BorderSize = 2;
                btnCreditos.FlatAppearance.BorderSize = 2;
                btnVentas.FlatAppearance.BorderSize = 2;
                ipbLogo.Visible = true;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            panelDatosCredito.Show();
        }

        private void txtCredito_Leave(object sender, EventArgs e)
        {
            if (txtCredito.Text == "")
            {
                double.Parse(txtCredito.Text = "0.00");
            }
        }

        private void txtTarjeta_Leave(object sender, EventArgs e)
        {
            if (txtTarjeta.Text == "")
            {
                double.Parse(txtTarjeta.Text = "0.00");
            }
        }

        private void txtEfectivo_Leave(object sender, EventArgs e)
        {
            if (txtEfectivo.Text == "")
            {
                double.Parse(txtEfectivo.Text = "0.00");
            }
        }
    }
}
