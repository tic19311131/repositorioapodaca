﻿namespace pdv_uth_v1
{
    partial class FrmPersonaSecundaria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPersonaSecundaria));
            this.panelForm = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelFormulario = new System.Windows.Forms.Panel();
            this.lblCurpClientes = new System.Windows.Forms.Label();
            this.lblDomClientes = new System.Windows.Forms.Label();
            this.lblIneClientes = new System.Windows.Forms.Label();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtFraccionamiento = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.txtApMat = new System.Windows.Forms.TextBox();
            this.txtApPat = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.panelFoto = new System.Windows.Forms.Panel();
            this.btnComprobantes = new System.Windows.Forms.Button();
            this.txtComprobanteCurp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtComproINE = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCurp = new System.Windows.Forms.TextBox();
            this.txtComprobanteDom = new System.Windows.Forms.Label();
            this.txtComprobanteDomicilio = new System.Windows.Forms.TextBox();
            this.lblCalle = new System.Windows.Forms.Label();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.txtNumCasa = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.panelBotones = new System.Windows.Forms.Panel();
            this.panel = new System.Windows.Forms.Panel();
            this.btnEliminarcliente = new System.Windows.Forms.Button();
            this.btnModificarCliente = new System.Windows.Forms.Button();
            this.btnGuardarCliente = new System.Windows.Forms.Button();
            this.panelTop = new System.Windows.Forms.Panel();
            this.btnRetroceder = new System.Windows.Forms.Button();
            this.Cerrar = new System.Windows.Forms.Button();
            this.lblNombrepersonaSecundaria = new System.Windows.Forms.Label();
            this.picUserImage = new System.Windows.Forms.PictureBox();
            this.btnApagar = new System.Windows.Forms.Button();
            this.btnAñadirClientes = new System.Windows.Forms.Button();
            this.panelForm.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelFormulario.SuspendLayout();
            this.panelBotones.SuspendLayout();
            this.panel.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.Color.Maroon;
            this.panelForm.Controls.Add(this.panel1);
            this.panelForm.Controls.Add(this.btnApagar);
            this.panelForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelForm.Location = new System.Drawing.Point(0, 0);
            this.panelForm.Margin = new System.Windows.Forms.Padding(2);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(800, 478);
            this.panelForm.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panelBotones);
            this.panel1.Controls.Add(this.panelTop);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 478);
            this.panel1.TabIndex = 45;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panelFormulario);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 116);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(670, 362);
            this.panel3.TabIndex = 46;
            // 
            // panelFormulario
            // 
            this.panelFormulario.BackColor = System.Drawing.Color.Gray;
            this.panelFormulario.Controls.Add(this.lblCurpClientes);
            this.panelFormulario.Controls.Add(this.lblDomClientes);
            this.panelFormulario.Controls.Add(this.lblIneClientes);
            this.panelFormulario.Controls.Add(this.dtpFechaNacimiento);
            this.panelFormulario.Controls.Add(this.label30);
            this.panelFormulario.Controls.Add(this.label31);
            this.panelFormulario.Controls.Add(this.label32);
            this.panelFormulario.Controls.Add(this.label33);
            this.panelFormulario.Controls.Add(this.txtTelefono);
            this.panelFormulario.Controls.Add(this.txtFraccionamiento);
            this.panelFormulario.Controls.Add(this.txtMunicipio);
            this.panelFormulario.Controls.Add(this.label36);
            this.panelFormulario.Controls.Add(this.label37);
            this.panelFormulario.Controls.Add(this.label38);
            this.panelFormulario.Controls.Add(this.lblNombre);
            this.panelFormulario.Controls.Add(this.txtCelular);
            this.panelFormulario.Controls.Add(this.txtApMat);
            this.panelFormulario.Controls.Add(this.txtApPat);
            this.panelFormulario.Controls.Add(this.txtNombre);
            this.panelFormulario.Controls.Add(this.panelFoto);
            this.panelFormulario.Controls.Add(this.btnComprobantes);
            this.panelFormulario.Controls.Add(this.txtComprobanteCurp);
            this.panelFormulario.Controls.Add(this.label1);
            this.panelFormulario.Controls.Add(this.txtComproINE);
            this.panelFormulario.Controls.Add(this.label26);
            this.panelFormulario.Controls.Add(this.txtLocalidad);
            this.panelFormulario.Controls.Add(this.label27);
            this.panelFormulario.Controls.Add(this.txtCurp);
            this.panelFormulario.Controls.Add(this.txtComprobanteDom);
            this.panelFormulario.Controls.Add(this.txtComprobanteDomicilio);
            this.panelFormulario.Controls.Add(this.lblCalle);
            this.panelFormulario.Controls.Add(this.txtCalle);
            this.panelFormulario.Controls.Add(this.label28);
            this.panelFormulario.Controls.Add(this.label29);
            this.panelFormulario.Controls.Add(this.txtCP);
            this.panelFormulario.Controls.Add(this.txtNumCasa);
            this.panelFormulario.Controls.Add(this.label34);
            this.panelFormulario.Controls.Add(this.label35);
            this.panelFormulario.Controls.Add(this.txtColonia);
            this.panelFormulario.Controls.Add(this.txtCorreo);
            this.panelFormulario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFormulario.Location = new System.Drawing.Point(0, 0);
            this.panelFormulario.Name = "panelFormulario";
            this.panelFormulario.Size = new System.Drawing.Size(670, 362);
            this.panelFormulario.TabIndex = 40;
            // 
            // lblCurpClientes
            // 
            this.lblCurpClientes.AutoSize = true;
            this.lblCurpClientes.BackColor = System.Drawing.Color.Transparent;
            this.lblCurpClientes.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurpClientes.ForeColor = System.Drawing.Color.Black;
            this.lblCurpClientes.Location = new System.Drawing.Point(206, 253);
            this.lblCurpClientes.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCurpClientes.Name = "lblCurpClientes";
            this.lblCurpClientes.Size = new System.Drawing.Size(136, 15);
            this.lblCurpClientes.TabIndex = 85;
            this.lblCurpClientes.Text = "Comprobante CURP";
            // 
            // lblDomClientes
            // 
            this.lblDomClientes.AutoSize = true;
            this.lblDomClientes.BackColor = System.Drawing.Color.Transparent;
            this.lblDomClientes.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDomClientes.ForeColor = System.Drawing.Color.Black;
            this.lblDomClientes.Location = new System.Drawing.Point(210, 201);
            this.lblDomClientes.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDomClientes.Name = "lblDomClientes";
            this.lblDomClientes.Size = new System.Drawing.Size(132, 15);
            this.lblDomClientes.TabIndex = 86;
            this.lblDomClientes.Text = "Comprobante Dom";
            // 
            // lblIneClientes
            // 
            this.lblIneClientes.AutoSize = true;
            this.lblIneClientes.BackColor = System.Drawing.Color.Transparent;
            this.lblIneClientes.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIneClientes.ForeColor = System.Drawing.Color.Black;
            this.lblIneClientes.Location = new System.Drawing.Point(211, 302);
            this.lblIneClientes.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIneClientes.Name = "lblIneClientes";
            this.lblIneClientes.Size = new System.Drawing.Size(121, 15);
            this.lblIneClientes.TabIndex = 87;
            this.lblIneClientes.Text = "Comprobante INE";
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(286, 37);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(108, 20);
            this.dtpFechaNacimiento.TabIndex = 72;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(308, 141);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(58, 14);
            this.label30.TabIndex = 83;
            this.label30.Text = "Telefono";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(290, 103);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(101, 14);
            this.label31.TabIndex = 82;
            this.label31.Text = "Fraccionamiento";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(291, 65);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 14);
            this.label32.TabIndex = 81;
            this.label32.Text = "Municipio";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(283, 22);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(126, 14);
            this.label33.TabIndex = 80;
            this.label33.Text = "Fecha De Nacimiento";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(286, 166);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(108, 20);
            this.txtTelefono.TabIndex = 75;
            // 
            // txtFraccionamiento
            // 
            this.txtFraccionamiento.Location = new System.Drawing.Point(286, 119);
            this.txtFraccionamiento.Name = "txtFraccionamiento";
            this.txtFraccionamiento.Size = new System.Drawing.Size(108, 20);
            this.txtFraccionamiento.TabIndex = 74;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.Location = new System.Drawing.Point(286, 80);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(108, 20);
            this.txtMunicipio.TabIndex = 73;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(185, 141);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(51, 14);
            this.label36.TabIndex = 79;
            this.label36.Text = "Celular ";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(167, 103);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(102, 14);
            this.label37.TabIndex = 78;
            this.label37.Text = "Apellido Materno";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(168, 65);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(99, 14);
            this.label38.TabIndex = 77;
            this.label38.Text = "Apellido Paterno";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.Color.Black;
            this.lblNombre.Location = new System.Drawing.Point(185, 22);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(55, 14);
            this.lblNombre.TabIndex = 76;
            this.lblNombre.Text = "Nombre ";
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(163, 166);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(108, 20);
            this.txtCelular.TabIndex = 71;
            // 
            // txtApMat
            // 
            this.txtApMat.Location = new System.Drawing.Point(163, 120);
            this.txtApMat.Name = "txtApMat";
            this.txtApMat.Size = new System.Drawing.Size(108, 20);
            this.txtApMat.TabIndex = 70;
            // 
            // txtApPat
            // 
            this.txtApPat.Location = new System.Drawing.Point(163, 80);
            this.txtApPat.Name = "txtApPat";
            this.txtApPat.Size = new System.Drawing.Size(108, 20);
            this.txtApPat.TabIndex = 69;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(163, 40);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(108, 20);
            this.txtNombre.TabIndex = 68;
            // 
            // panelFoto
            // 
            this.panelFoto.BackColor = System.Drawing.Color.Red;
            this.panelFoto.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelFoto.Location = new System.Drawing.Point(0, 0);
            this.panelFoto.Name = "panelFoto";
            this.panelFoto.Size = new System.Drawing.Size(154, 362);
            this.panelFoto.TabIndex = 67;
            // 
            // btnComprobantes
            // 
            this.btnComprobantes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnComprobantes.BackColor = System.Drawing.Color.Gray;
            this.btnComprobantes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnComprobantes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnComprobantes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnComprobantes.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComprobantes.ForeColor = System.Drawing.Color.Black;
            this.btnComprobantes.Image = ((System.Drawing.Image)(resources.GetObject("btnComprobantes.Image")));
            this.btnComprobantes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnComprobantes.Location = new System.Drawing.Point(404, 254);
            this.btnComprobantes.Name = "btnComprobantes";
            this.btnComprobantes.Size = new System.Drawing.Size(233, 70);
            this.btnComprobantes.TabIndex = 60;
            this.btnComprobantes.Text = "                Añadir                      Comprobantes";
            this.btnComprobantes.UseVisualStyleBackColor = false;
            this.btnComprobantes.Click += new System.EventHandler(this.btnComprobantes_Click);
            // 
            // txtComprobanteCurp
            // 
            this.txtComprobanteCurp.Location = new System.Drawing.Point(539, 206);
            this.txtComprobanteCurp.Name = "txtComprobanteCurp";
            this.txtComprobanteCurp.Size = new System.Drawing.Size(108, 20);
            this.txtComprobanteCurp.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(536, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 14);
            this.label1.TabIndex = 52;
            this.label1.Text = "Comprobante INE";
            // 
            // txtComproINE
            // 
            this.txtComproINE.Location = new System.Drawing.Point(537, 166);
            this.txtComproINE.Name = "txtComproINE";
            this.txtComproINE.Size = new System.Drawing.Size(108, 20);
            this.txtComproINE.TabIndex = 16;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(439, 148);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(60, 14);
            this.label26.TabIndex = 50;
            this.label26.Text = "Localidad";
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.Location = new System.Drawing.Point(413, 166);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(108, 20);
            this.txtLocalidad.TabIndex = 7;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(450, 189);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(39, 14);
            this.label27.TabIndex = 48;
            this.label27.Text = "CURP";
            // 
            // txtCurp
            // 
            this.txtCurp.Location = new System.Drawing.Point(413, 206);
            this.txtCurp.Name = "txtCurp";
            this.txtCurp.Size = new System.Drawing.Size(108, 20);
            this.txtCurp.TabIndex = 8;
            // 
            // txtComprobanteDom
            // 
            this.txtComprobanteDom.AutoSize = true;
            this.txtComprobanteDom.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComprobanteDom.ForeColor = System.Drawing.Color.Black;
            this.txtComprobanteDom.Location = new System.Drawing.Point(533, 102);
            this.txtComprobanteDom.Name = "txtComprobanteDom";
            this.txtComprobanteDom.Size = new System.Drawing.Size(113, 14);
            this.txtComprobanteDom.TabIndex = 46;
            this.txtComprobanteDom.Text = "Comprobante dom";
            // 
            // txtComprobanteDomicilio
            // 
            this.txtComprobanteDomicilio.Location = new System.Drawing.Point(537, 119);
            this.txtComprobanteDomicilio.Name = "txtComprobanteDomicilio";
            this.txtComprobanteDomicilio.Size = new System.Drawing.Size(108, 20);
            this.txtComprobanteDomicilio.TabIndex = 15;
            // 
            // lblCalle
            // 
            this.lblCalle.AutoSize = true;
            this.lblCalle.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalle.ForeColor = System.Drawing.Color.Black;
            this.lblCalle.Location = new System.Drawing.Point(450, 101);
            this.lblCalle.Name = "lblCalle";
            this.lblCalle.Size = new System.Drawing.Size(36, 14);
            this.lblCalle.TabIndex = 44;
            this.lblCalle.Text = "Calle";
            // 
            // txtCalle
            // 
            this.txtCalle.Location = new System.Drawing.Point(414, 119);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(108, 20);
            this.txtCalle.TabIndex = 6;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(543, 63);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 14);
            this.label28.TabIndex = 41;
            this.label28.Text = "Codigo Postal";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(536, 22);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 14);
            this.label29.TabIndex = 40;
            this.label29.Text = "Numero de Casa";
            // 
            // txtCP
            // 
            this.txtCP.Location = new System.Drawing.Point(538, 80);
            this.txtCP.Name = "txtCP";
            this.txtCP.Size = new System.Drawing.Size(108, 20);
            this.txtCP.TabIndex = 14;
            // 
            // txtNumCasa
            // 
            this.txtNumCasa.Location = new System.Drawing.Point(538, 40);
            this.txtNumCasa.Name = "txtNumCasa";
            this.txtNumCasa.Size = new System.Drawing.Size(108, 20);
            this.txtNumCasa.TabIndex = 13;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(441, 62);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(50, 14);
            this.label34.TabIndex = 29;
            this.label34.Text = "Colonia";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(413, 22);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(112, 14);
            this.label35.TabIndex = 28;
            this.label35.Text = "Correo Electronico";
            // 
            // txtColonia
            // 
            this.txtColonia.Location = new System.Drawing.Point(415, 80);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(108, 20);
            this.txtColonia.TabIndex = 5;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(415, 40);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(108, 20);
            this.txtCorreo.TabIndex = 4;
            // 
            // panelBotones
            // 
            this.panelBotones.Controls.Add(this.panel);
            this.panelBotones.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelBotones.Location = new System.Drawing.Point(670, 116);
            this.panelBotones.Name = "panelBotones";
            this.panelBotones.Size = new System.Drawing.Size(130, 362);
            this.panelBotones.TabIndex = 45;
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.Gray;
            this.panel.Controls.Add(this.btnEliminarcliente);
            this.panel.Controls.Add(this.btnModificarCliente);
            this.panel.Controls.Add(this.btnGuardarCliente);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(130, 362);
            this.panel.TabIndex = 44;
            // 
            // btnEliminarcliente
            // 
            this.btnEliminarcliente.BackColor = System.Drawing.Color.Gray;
            this.btnEliminarcliente.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnEliminarcliente.FlatAppearance.BorderSize = 2;
            this.btnEliminarcliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnEliminarcliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnEliminarcliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarcliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarcliente.ForeColor = System.Drawing.Color.Black;
            this.btnEliminarcliente.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarcliente.Image")));
            this.btnEliminarcliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarcliente.Location = new System.Drawing.Point(0, 287);
            this.btnEliminarcliente.Name = "btnEliminarcliente";
            this.btnEliminarcliente.Size = new System.Drawing.Size(130, 75);
            this.btnEliminarcliente.TabIndex = 39;
            this.btnEliminarcliente.Text = "    Eliminar  Clientes";
            this.btnEliminarcliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminarcliente.UseVisualStyleBackColor = false;
            // 
            // btnModificarCliente
            // 
            this.btnModificarCliente.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnModificarCliente.FlatAppearance.BorderSize = 2;
            this.btnModificarCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnModificarCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnModificarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificarCliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarCliente.ForeColor = System.Drawing.Color.Black;
            this.btnModificarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnModificarCliente.Image")));
            this.btnModificarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificarCliente.Location = new System.Drawing.Point(0, 0);
            this.btnModificarCliente.Name = "btnModificarCliente";
            this.btnModificarCliente.Size = new System.Drawing.Size(130, 75);
            this.btnModificarCliente.TabIndex = 42;
            this.btnModificarCliente.Text = "Modificar Cliente";
            this.btnModificarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificarCliente.UseVisualStyleBackColor = true;
            // 
            // btnGuardarCliente
            // 
            this.btnGuardarCliente.FlatAppearance.BorderSize = 2;
            this.btnGuardarCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnGuardarCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnGuardarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarCliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarCliente.ForeColor = System.Drawing.Color.Black;
            this.btnGuardarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardarCliente.Image")));
            this.btnGuardarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarCliente.Location = new System.Drawing.Point(0, 146);
            this.btnGuardarCliente.Name = "btnGuardarCliente";
            this.btnGuardarCliente.Size = new System.Drawing.Size(130, 75);
            this.btnGuardarCliente.TabIndex = 40;
            this.btnGuardarCliente.Text = "Guardar\r\nCliente";
            this.btnGuardarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardarCliente.UseVisualStyleBackColor = true;
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.Gray;
            this.panelTop.Controls.Add(this.btnAñadirClientes);
            this.panelTop.Controls.Add(this.btnRetroceder);
            this.panelTop.Controls.Add(this.Cerrar);
            this.panelTop.Controls.Add(this.lblNombrepersonaSecundaria);
            this.panelTop.Controls.Add(this.picUserImage);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(800, 116);
            this.panelTop.TabIndex = 42;
            // 
            // btnRetroceder
            // 
            this.btnRetroceder.FlatAppearance.BorderSize = 0;
            this.btnRetroceder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetroceder.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetroceder.Image = ((System.Drawing.Image)(resources.GetObject("btnRetroceder.Image")));
            this.btnRetroceder.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnRetroceder.Location = new System.Drawing.Point(0, 3);
            this.btnRetroceder.Name = "btnRetroceder";
            this.btnRetroceder.Size = new System.Drawing.Size(112, 136);
            this.btnRetroceder.TabIndex = 48;
            this.btnRetroceder.UseVisualStyleBackColor = true;
            this.btnRetroceder.Click += new System.EventHandler(this.btnRetroceder_Click);
            // 
            // Cerrar
            // 
            this.Cerrar.Dock = System.Windows.Forms.DockStyle.Right;
            this.Cerrar.FlatAppearance.BorderSize = 0;
            this.Cerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.Cerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cerrar.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cerrar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Cerrar.Image = ((System.Drawing.Image)(resources.GetObject("Cerrar.Image")));
            this.Cerrar.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.Cerrar.Location = new System.Drawing.Point(726, 0);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(74, 116);
            this.Cerrar.TabIndex = 47;
            this.Cerrar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Cerrar.UseVisualStyleBackColor = true;
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // lblNombrepersonaSecundaria
            // 
            this.lblNombrepersonaSecundaria.AutoSize = true;
            this.lblNombrepersonaSecundaria.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombrepersonaSecundaria.ForeColor = System.Drawing.Color.Black;
            this.lblNombrepersonaSecundaria.Location = new System.Drawing.Point(300, 97);
            this.lblNombrepersonaSecundaria.Name = "lblNombrepersonaSecundaria";
            this.lblNombrepersonaSecundaria.Size = new System.Drawing.Size(192, 14);
            this.lblNombrepersonaSecundaria.TabIndex = 23;
            this.lblNombrepersonaSecundaria.Text = "Nonmbre  La Persona Secundaria";
            // 
            // picUserImage
            // 
            this.picUserImage.Image = ((System.Drawing.Image)(resources.GetObject("picUserImage.Image")));
            this.picUserImage.Location = new System.Drawing.Point(356, 3);
            this.picUserImage.Name = "picUserImage";
            this.picUserImage.Size = new System.Drawing.Size(90, 91);
            this.picUserImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picUserImage.TabIndex = 22;
            this.picUserImage.TabStop = false;
            // 
            // btnApagar
            // 
            this.btnApagar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApagar.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnApagar.Location = new System.Drawing.Point(485, -114);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(53, 54);
            this.btnApagar.TabIndex = 37;
            this.btnApagar.Text = "Salir";
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnApagar.UseVisualStyleBackColor = true;
            // 
            // btnAñadirClientes
            // 
            this.btnAñadirClientes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAñadirClientes.BackColor = System.Drawing.Color.Gray;
            this.btnAñadirClientes.FlatAppearance.BorderSize = 2;
            this.btnAñadirClientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAñadirClientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnAñadirClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAñadirClientes.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAñadirClientes.ForeColor = System.Drawing.Color.Black;
            this.btnAñadirClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnAñadirClientes.Image")));
            this.btnAñadirClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAñadirClientes.Location = new System.Drawing.Point(594, 27);
            this.btnAñadirClientes.Name = "btnAñadirClientes";
            this.btnAñadirClientes.Size = new System.Drawing.Size(116, 67);
            this.btnAñadirClientes.TabIndex = 49;
            this.btnAñadirClientes.Text = "        Añadir\r\n       Clientes";
            this.btnAñadirClientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAñadirClientes.UseVisualStyleBackColor = false;
            // 
            // FrmPersonaSecundaria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(800, 478);
            this.Controls.Add(this.panelForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPersonaSecundaria";
            this.Text = "FrmPersonaSecundaria";
            this.panelForm.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panelFormulario.ResumeLayout(false);
            this.panelFormulario.PerformLayout();
            this.panelBotones.ResumeLayout(false);
            this.panel.ResumeLayout(false);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Label lblNombrepersonaSecundaria;
        private System.Windows.Forms.PictureBox picUserImage;
        private System.Windows.Forms.Panel panelFormulario;
        private System.Windows.Forms.TextBox txtComprobanteCurp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtComproINE;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtLocalidad;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCurp;
        private System.Windows.Forms.Label txtComprobanteDom;
        private System.Windows.Forms.TextBox txtComprobanteDomicilio;
        private System.Windows.Forms.Label lblCalle;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtCP;
        private System.Windows.Forms.TextBox txtNumCasa;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtColonia;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.Button Cerrar;
        private System.Windows.Forms.Button btnRetroceder;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panelBotones;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button btnEliminarcliente;
        private System.Windows.Forms.Button btnModificarCliente;
        private System.Windows.Forms.Button btnGuardarCliente;
        private System.Windows.Forms.Button btnComprobantes;
        private System.Windows.Forms.Panel panelFoto;
        private System.Windows.Forms.Label lblCurpClientes;
        private System.Windows.Forms.Label lblDomClientes;
        private System.Windows.Forms.Label lblIneClientes;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtFraccionamiento;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.TextBox txtApMat;
        private System.Windows.Forms.TextBox txtApPat;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button btnAñadirClientes;
    }
}