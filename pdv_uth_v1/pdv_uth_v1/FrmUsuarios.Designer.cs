﻿namespace pdv_uth_v1
{
    partial class FrmUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUsuarios));
            this.panelForm = new System.Windows.Forms.Panel();
            this.btnRetroceder = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAñadirClientes = new System.Windows.Forms.Button();
            this.btnEliminarclientes = new System.Windows.Forms.Button();
            this.btnModificarCliente = new System.Windows.Forms.Button();
            this.btnGuardarCliente = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblNombreCliente = new System.Windows.Forms.Label();
            this.picUserImage = new System.Windows.Forms.PictureBox();
            this.panelFormulario = new System.Windows.Forms.Panel();
            this.cbTipoUser = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUsuarioContra = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNumSS = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCertEstudios = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtActaDeNac = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtComprobanteCurp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtComproINE = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCurp = new System.Windows.Forms.TextBox();
            this.txtComprobanteDom = new System.Windows.Forms.Label();
            this.txtComprobanteDomicilio = new System.Windows.Forms.TextBox();
            this.lblCalle = new System.Windows.Forms.Label();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.txtNumCasa = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtFraccionamiento = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.txtApMat = new System.Windows.Forms.TextBox();
            this.txtApPat = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgUsuarios = new System.Windows.Forms.DataGridView();
            this.panelForm.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).BeginInit();
            this.panelFormulario.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUsuarios)).BeginInit();
            this.SuspendLayout();
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.Color.Gray;
            this.panelForm.Controls.Add(this.btnRetroceder);
            this.panelForm.Controls.Add(this.panel1);
            this.panelForm.Controls.Add(this.btnApagar);
            this.panelForm.Controls.Add(this.panel5);
            this.panelForm.Controls.Add(this.panelFormulario);
            this.panelForm.Controls.Add(this.btnLimpiar);
            this.panelForm.Controls.Add(this.btnBorrar);
            this.panelForm.Controls.Add(this.btnModificar);
            this.panelForm.Controls.Add(this.btnGuardar);
            this.panelForm.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelForm.Location = new System.Drawing.Point(0, 0);
            this.panelForm.Margin = new System.Windows.Forms.Padding(2);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(402, 749);
            this.panelForm.TabIndex = 1;
            // 
            // btnRetroceder
            // 
            this.btnRetroceder.FlatAppearance.BorderSize = 0;
            this.btnRetroceder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetroceder.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetroceder.Image = ((System.Drawing.Image)(resources.GetObject("btnRetroceder.Image")));
            this.btnRetroceder.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnRetroceder.Location = new System.Drawing.Point(0, 0);
            this.btnRetroceder.Name = "btnRetroceder";
            this.btnRetroceder.Size = new System.Drawing.Size(69, 66);
            this.btnRetroceder.TabIndex = 49;
            this.btnRetroceder.UseVisualStyleBackColor = true;
            this.btnRetroceder.Click += new System.EventHandler(this.btnRetroceder_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnAñadirClientes);
            this.panel1.Controls.Add(this.btnEliminarclientes);
            this.panel1.Controls.Add(this.btnModificarCliente);
            this.panel1.Controls.Add(this.btnGuardarCliente);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 557);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(402, 192);
            this.panel1.TabIndex = 43;
            // 
            // btnAñadirClientes
            // 
            this.btnAñadirClientes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAñadirClientes.BackColor = System.Drawing.Color.Gray;
            this.btnAñadirClientes.FlatAppearance.BorderSize = 2;
            this.btnAñadirClientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAñadirClientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnAñadirClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAñadirClientes.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAñadirClientes.ForeColor = System.Drawing.Color.Black;
            this.btnAñadirClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnAñadirClientes.Image")));
            this.btnAñadirClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAñadirClientes.Location = new System.Drawing.Point(37, 21);
            this.btnAñadirClientes.Name = "btnAñadirClientes";
            this.btnAñadirClientes.Size = new System.Drawing.Size(137, 57);
            this.btnAñadirClientes.TabIndex = 38;
            this.btnAñadirClientes.Text = "     Añadir   Usuario";
            this.btnAñadirClientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAñadirClientes.UseVisualStyleBackColor = false;
            // 
            // btnEliminarclientes
            // 
            this.btnEliminarclientes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEliminarclientes.BackColor = System.Drawing.Color.Gray;
            this.btnEliminarclientes.FlatAppearance.BorderSize = 2;
            this.btnEliminarclientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnEliminarclientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnEliminarclientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarclientes.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarclientes.ForeColor = System.Drawing.Color.Black;
            this.btnEliminarclientes.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarclientes.Image")));
            this.btnEliminarclientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarclientes.Location = new System.Drawing.Point(37, 93);
            this.btnEliminarclientes.Name = "btnEliminarclientes";
            this.btnEliminarclientes.Size = new System.Drawing.Size(137, 57);
            this.btnEliminarclientes.TabIndex = 39;
            this.btnEliminarclientes.Text = "Eliminar    Usuarios";
            this.btnEliminarclientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminarclientes.UseVisualStyleBackColor = false;
            // 
            // btnModificarCliente
            // 
            this.btnModificarCliente.BackColor = System.Drawing.Color.Gray;
            this.btnModificarCliente.FlatAppearance.BorderSize = 2;
            this.btnModificarCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnModificarCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnModificarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificarCliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarCliente.ForeColor = System.Drawing.Color.Black;
            this.btnModificarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnModificarCliente.Image")));
            this.btnModificarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificarCliente.Location = new System.Drawing.Point(217, 21);
            this.btnModificarCliente.Name = "btnModificarCliente";
            this.btnModificarCliente.Size = new System.Drawing.Size(137, 57);
            this.btnModificarCliente.TabIndex = 42;
            this.btnModificarCliente.Text = "Modificar Usuario";
            this.btnModificarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificarCliente.UseVisualStyleBackColor = false;
            // 
            // btnGuardarCliente
            // 
            this.btnGuardarCliente.BackColor = System.Drawing.Color.Gray;
            this.btnGuardarCliente.FlatAppearance.BorderSize = 2;
            this.btnGuardarCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnGuardarCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnGuardarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarCliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarCliente.ForeColor = System.Drawing.Color.Black;
            this.btnGuardarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardarCliente.Image")));
            this.btnGuardarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarCliente.Location = new System.Drawing.Point(217, 93);
            this.btnGuardarCliente.Name = "btnGuardarCliente";
            this.btnGuardarCliente.Size = new System.Drawing.Size(137, 57);
            this.btnGuardarCliente.TabIndex = 40;
            this.btnGuardarCliente.Text = "  Guardar  Usuario";
            this.btnGuardarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardarCliente.UseVisualStyleBackColor = false;
            // 
            // btnApagar
            // 
            this.btnApagar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnApagar.BackColor = System.Drawing.Color.Gray;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApagar.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.ForeColor = System.Drawing.Color.Black;
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnApagar.Location = new System.Drawing.Point(532, 0);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(69, 69);
            this.btnApagar.TabIndex = 37;
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnApagar.UseVisualStyleBackColor = false;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblNombreCliente);
            this.panel5.Controls.Add(this.picUserImage);
            this.panel5.Location = new System.Drawing.Point(2, 67);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(403, 111);
            this.panel5.TabIndex = 42;
            // 
            // lblNombreCliente
            // 
            this.lblNombreCliente.AutoSize = true;
            this.lblNombreCliente.BackColor = System.Drawing.Color.Gray;
            this.lblNombreCliente.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreCliente.ForeColor = System.Drawing.Color.Black;
            this.lblNombreCliente.Location = new System.Drawing.Point(142, 94);
            this.lblNombreCliente.Name = "lblNombreCliente";
            this.lblNombreCliente.Size = new System.Drawing.Size(127, 14);
            this.lblNombreCliente.TabIndex = 23;
            this.lblNombreCliente.Text = "Nonmbre  Del Cliente";
            // 
            // picUserImage
            // 
            this.picUserImage.Image = ((System.Drawing.Image)(resources.GetObject("picUserImage.Image")));
            this.picUserImage.Location = new System.Drawing.Point(162, 0);
            this.picUserImage.Name = "picUserImage";
            this.picUserImage.Size = new System.Drawing.Size(90, 91);
            this.picUserImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picUserImage.TabIndex = 22;
            this.picUserImage.TabStop = false;
            // 
            // panelFormulario
            // 
            this.panelFormulario.Controls.Add(this.cbTipoUser);
            this.panelFormulario.Controls.Add(this.label7);
            this.panelFormulario.Controls.Add(this.txtUsuarioContra);
            this.panelFormulario.Controls.Add(this.label4);
            this.panelFormulario.Controls.Add(this.label5);
            this.panelFormulario.Controls.Add(this.txtNumSS);
            this.panelFormulario.Controls.Add(this.label3);
            this.panelFormulario.Controls.Add(this.txtCertEstudios);
            this.panelFormulario.Controls.Add(this.label2);
            this.panelFormulario.Controls.Add(this.txtActaDeNac);
            this.panelFormulario.Controls.Add(this.label25);
            this.panelFormulario.Controls.Add(this.txtComprobanteCurp);
            this.panelFormulario.Controls.Add(this.label1);
            this.panelFormulario.Controls.Add(this.txtComproINE);
            this.panelFormulario.Controls.Add(this.label26);
            this.panelFormulario.Controls.Add(this.txtLocalidad);
            this.panelFormulario.Controls.Add(this.label27);
            this.panelFormulario.Controls.Add(this.txtCurp);
            this.panelFormulario.Controls.Add(this.txtComprobanteDom);
            this.panelFormulario.Controls.Add(this.txtComprobanteDomicilio);
            this.panelFormulario.Controls.Add(this.lblCalle);
            this.panelFormulario.Controls.Add(this.txtCalle);
            this.panelFormulario.Controls.Add(this.dtpFechaNacimiento);
            this.panelFormulario.Controls.Add(this.label28);
            this.panelFormulario.Controls.Add(this.label29);
            this.panelFormulario.Controls.Add(this.label30);
            this.panelFormulario.Controls.Add(this.label31);
            this.panelFormulario.Controls.Add(this.label32);
            this.panelFormulario.Controls.Add(this.label33);
            this.panelFormulario.Controls.Add(this.txtCP);
            this.panelFormulario.Controls.Add(this.txtNumCasa);
            this.panelFormulario.Controls.Add(this.txtTelefono);
            this.panelFormulario.Controls.Add(this.txtFraccionamiento);
            this.panelFormulario.Controls.Add(this.txtMunicipio);
            this.panelFormulario.Controls.Add(this.label34);
            this.panelFormulario.Controls.Add(this.label35);
            this.panelFormulario.Controls.Add(this.label36);
            this.panelFormulario.Controls.Add(this.label37);
            this.panelFormulario.Controls.Add(this.label38);
            this.panelFormulario.Controls.Add(this.lblNombre);
            this.panelFormulario.Controls.Add(this.txtColonia);
            this.panelFormulario.Controls.Add(this.txtCorreo);
            this.panelFormulario.Controls.Add(this.txtCelular);
            this.panelFormulario.Controls.Add(this.txtApMat);
            this.panelFormulario.Controls.Add(this.txtApPat);
            this.panelFormulario.Controls.Add(this.txtNombre);
            this.panelFormulario.Location = new System.Drawing.Point(3, 181);
            this.panelFormulario.Name = "panelFormulario";
            this.panelFormulario.Size = new System.Drawing.Size(409, 386);
            this.panelFormulario.TabIndex = 40;
            this.panelFormulario.Paint += new System.Windows.Forms.PaintEventHandler(this.panelFormulario_Paint);
            // 
            // cbTipoUser
            // 
            this.cbTipoUser.FormattingEnabled = true;
            this.cbTipoUser.Location = new System.Drawing.Point(259, 225);
            this.cbTipoUser.Name = "cbTipoUser";
            this.cbTipoUser.Size = new System.Drawing.Size(110, 21);
            this.cbTipoUser.TabIndex = 65;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Gray;
            this.label7.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(258, 245);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 14);
            this.label7.TabIndex = 64;
            this.label7.Text = "Contraseña";
            // 
            // txtUsuarioContra
            // 
            this.txtUsuarioContra.Location = new System.Drawing.Point(261, 262);
            this.txtUsuarioContra.Name = "txtUsuarioContra";
            this.txtUsuarioContra.Size = new System.Drawing.Size(108, 20);
            this.txtUsuarioContra.TabIndex = 63;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Gray;
            this.label4.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(258, 205);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 14);
            this.label4.TabIndex = 62;
            this.label4.Text = "Tipo de Usuario";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Gray;
            this.label5.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(258, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 14);
            this.label5.TabIndex = 60;
            this.label5.Text = "Num Seguro  Social";
            // 
            // txtNumSS
            // 
            this.txtNumSS.Location = new System.Drawing.Point(261, 182);
            this.txtNumSS.Name = "txtNumSS";
            this.txtNumSS.Size = new System.Drawing.Size(108, 20);
            this.txtNumSS.TabIndex = 59;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Gray;
            this.label3.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(251, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 14);
            this.label3.TabIndex = 58;
            this.label3.Text = "Certificado De Estudios";
            // 
            // txtCertEstudios
            // 
            this.txtCertEstudios.Location = new System.Drawing.Point(261, 145);
            this.txtCertEstudios.Name = "txtCertEstudios";
            this.txtCertEstudios.Size = new System.Drawing.Size(108, 20);
            this.txtCertEstudios.TabIndex = 57;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Gray;
            this.label2.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(258, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 14);
            this.label2.TabIndex = 56;
            this.label2.Text = "Acta de  Nacimiento";
            // 
            // txtActaDeNac
            // 
            this.txtActaDeNac.Location = new System.Drawing.Point(261, 105);
            this.txtActaDeNac.Name = "txtActaDeNac";
            this.txtActaDeNac.Size = new System.Drawing.Size(108, 20);
            this.txtActaDeNac.TabIndex = 55;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Gray;
            this.label25.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(131, 333);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(120, 14);
            this.label25.TabIndex = 54;
            this.label25.Text = "Comprobante CURP";
            // 
            // txtComprobanteCurp
            // 
            this.txtComprobanteCurp.Location = new System.Drawing.Point(134, 350);
            this.txtComprobanteCurp.Name = "txtComprobanteCurp";
            this.txtComprobanteCurp.Size = new System.Drawing.Size(108, 20);
            this.txtComprobanteCurp.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Gray;
            this.label1.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(132, 292);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 14);
            this.label1.TabIndex = 52;
            this.label1.Text = "Comprobante INE";
            // 
            // txtComproINE
            // 
            this.txtComproINE.Location = new System.Drawing.Point(133, 311);
            this.txtComproINE.Name = "txtComproINE";
            this.txtComproINE.Size = new System.Drawing.Size(108, 20);
            this.txtComproINE.TabIndex = 16;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Gray;
            this.label26.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(35, 293);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(60, 14);
            this.label26.TabIndex = 50;
            this.label26.Text = "Localidad";
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.Location = new System.Drawing.Point(13, 311);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(108, 20);
            this.txtLocalidad.TabIndex = 7;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Gray;
            this.label27.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(46, 334);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(39, 14);
            this.label27.TabIndex = 48;
            this.label27.Text = "CURP";
            // 
            // txtCurp
            // 
            this.txtCurp.Location = new System.Drawing.Point(12, 351);
            this.txtCurp.Name = "txtCurp";
            this.txtCurp.Size = new System.Drawing.Size(108, 20);
            this.txtCurp.TabIndex = 8;
            // 
            // txtComprobanteDom
            // 
            this.txtComprobanteDom.AutoSize = true;
            this.txtComprobanteDom.BackColor = System.Drawing.Color.Gray;
            this.txtComprobanteDom.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComprobanteDom.ForeColor = System.Drawing.Color.Black;
            this.txtComprobanteDom.Location = new System.Drawing.Point(129, 247);
            this.txtComprobanteDom.Name = "txtComprobanteDom";
            this.txtComprobanteDom.Size = new System.Drawing.Size(113, 14);
            this.txtComprobanteDom.TabIndex = 46;
            this.txtComprobanteDom.Text = "Comprobante dom";
            // 
            // txtComprobanteDomicilio
            // 
            this.txtComprobanteDomicilio.Location = new System.Drawing.Point(133, 264);
            this.txtComprobanteDomicilio.Name = "txtComprobanteDomicilio";
            this.txtComprobanteDomicilio.Size = new System.Drawing.Size(108, 20);
            this.txtComprobanteDomicilio.TabIndex = 15;
            // 
            // lblCalle
            // 
            this.lblCalle.AutoSize = true;
            this.lblCalle.BackColor = System.Drawing.Color.Gray;
            this.lblCalle.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalle.ForeColor = System.Drawing.Color.Black;
            this.lblCalle.Location = new System.Drawing.Point(46, 246);
            this.lblCalle.Name = "lblCalle";
            this.lblCalle.Size = new System.Drawing.Size(36, 14);
            this.lblCalle.TabIndex = 44;
            this.lblCalle.Text = "Calle";
            // 
            // txtCalle
            // 
            this.txtCalle.Location = new System.Drawing.Point(10, 264);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(108, 20);
            this.txtCalle.TabIndex = 6;
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(134, 29);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(108, 20);
            this.dtpFechaNacimiento.TabIndex = 9;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Gray;
            this.label28.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(139, 208);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 14);
            this.label28.TabIndex = 41;
            this.label28.Text = "Codigo Postal";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Gray;
            this.label29.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(132, 167);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 14);
            this.label29.TabIndex = 40;
            this.label29.Text = "Numero de Casa";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Gray;
            this.label30.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(156, 126);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(58, 14);
            this.label30.TabIndex = 39;
            this.label30.Text = "Telefono";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Gray;
            this.label31.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(138, 88);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(101, 14);
            this.label31.TabIndex = 38;
            this.label31.Text = "Fraccionamiento";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Gray;
            this.label32.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(139, 50);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 14);
            this.label32.TabIndex = 37;
            this.label32.Text = "Municipio";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Gray;
            this.label33.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(125, 14);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(126, 14);
            this.label33.TabIndex = 36;
            this.label33.Text = "Fecha De Nacimiento";
            // 
            // txtCP
            // 
            this.txtCP.Location = new System.Drawing.Point(134, 225);
            this.txtCP.Name = "txtCP";
            this.txtCP.Size = new System.Drawing.Size(108, 20);
            this.txtCP.TabIndex = 14;
            // 
            // txtNumCasa
            // 
            this.txtNumCasa.Location = new System.Drawing.Point(134, 185);
            this.txtNumCasa.Name = "txtNumCasa";
            this.txtNumCasa.Size = new System.Drawing.Size(108, 20);
            this.txtNumCasa.TabIndex = 13;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(134, 144);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(108, 20);
            this.txtTelefono.TabIndex = 12;
            // 
            // txtFraccionamiento
            // 
            this.txtFraccionamiento.Location = new System.Drawing.Point(134, 105);
            this.txtFraccionamiento.Name = "txtFraccionamiento";
            this.txtFraccionamiento.Size = new System.Drawing.Size(108, 20);
            this.txtFraccionamiento.TabIndex = 11;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.Location = new System.Drawing.Point(134, 65);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(108, 20);
            this.txtMunicipio.TabIndex = 10;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Gray;
            this.label34.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(37, 207);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(50, 14);
            this.label34.TabIndex = 29;
            this.label34.Text = "Colonia";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Gray;
            this.label35.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(9, 167);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(112, 14);
            this.label35.TabIndex = 28;
            this.label35.Text = "Correo Electronico";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Gray;
            this.label36.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(33, 126);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(51, 14);
            this.label36.TabIndex = 27;
            this.label36.Text = "Celular ";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Gray;
            this.label37.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(15, 88);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(102, 14);
            this.label37.TabIndex = 26;
            this.label37.Text = "Apellido Materno";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Gray;
            this.label38.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(16, 50);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(99, 14);
            this.label38.TabIndex = 25;
            this.label38.Text = "Apellido Paterno";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.BackColor = System.Drawing.Color.Gray;
            this.lblNombre.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.Color.Black;
            this.lblNombre.Location = new System.Drawing.Point(33, 14);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(55, 14);
            this.lblNombre.TabIndex = 24;
            this.lblNombre.Text = "Nombre ";
            // 
            // txtColonia
            // 
            this.txtColonia.Location = new System.Drawing.Point(11, 225);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(108, 20);
            this.txtColonia.TabIndex = 5;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(11, 185);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(108, 20);
            this.txtCorreo.TabIndex = 4;
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(11, 144);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(108, 20);
            this.txtCelular.TabIndex = 3;
            // 
            // txtApMat
            // 
            this.txtApMat.Location = new System.Drawing.Point(11, 105);
            this.txtApMat.Name = "txtApMat";
            this.txtApMat.Size = new System.Drawing.Size(108, 20);
            this.txtApMat.TabIndex = 2;
            // 
            // txtApPat
            // 
            this.txtApPat.Location = new System.Drawing.Point(11, 65);
            this.txtApPat.Name = "txtApPat";
            this.txtApPat.Size = new System.Drawing.Size(108, 20);
            this.txtApPat.TabIndex = 1;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(11, 29);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(108, 20);
            this.txtNombre.TabIndex = 0;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(658, 195);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(2);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(56, 19);
            this.btnLimpiar.TabIndex = 31;
            this.btnLimpiar.Text = "LIMPIAR";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(658, 146);
            this.btnBorrar.Margin = new System.Windows.Forms.Padding(2);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(56, 19);
            this.btnBorrar.TabIndex = 30;
            this.btnBorrar.Text = "BORRAR";
            this.btnBorrar.UseVisualStyleBackColor = true;
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(658, 98);
            this.btnModificar.Margin = new System.Windows.Forms.Padding(2);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(56, 19);
            this.btnModificar.TabIndex = 29;
            this.btnModificar.Text = "MODIFICAR";
            this.btnModificar.UseVisualStyleBackColor = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(658, 50);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(2);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(56, 19);
            this.btnGuardar.TabIndex = 28;
            this.btnGuardar.Text = "ALTA";
            this.btnGuardar.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgUsuarios);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(402, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(0, 749);
            this.panel2.TabIndex = 2;
            // 
            // dgUsuarios
            // 
            this.dgUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUsuarios.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgUsuarios.Location = new System.Drawing.Point(0, 0);
            this.dgUsuarios.Name = "dgUsuarios";
            this.dgUsuarios.Size = new System.Drawing.Size(10, 749);
            this.dgUsuarios.TabIndex = 0;
            this.dgUsuarios.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUsuarios_CellContentClick);
            // 
            // FrmUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 749);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panelForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmUsuarios";
            this.Text = "FrmUsuarios";
            this.panelForm.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).EndInit();
            this.panelFormulario.ResumeLayout(false);
            this.panelFormulario.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgUsuarios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnEliminarclientes;
        private System.Windows.Forms.Button btnModificarCliente;
        private System.Windows.Forms.Button btnGuardarCliente;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnAñadirClientes;
        private System.Windows.Forms.Label lblNombreCliente;
        private System.Windows.Forms.PictureBox picUserImage;
        private System.Windows.Forms.Panel panelFormulario;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtComprobanteCurp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtComproINE;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtLocalidad;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCurp;
        private System.Windows.Forms.Label txtComprobanteDom;
        private System.Windows.Forms.TextBox txtComprobanteDomicilio;
        private System.Windows.Forms.Label lblCalle;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtCP;
        private System.Windows.Forms.TextBox txtNumCasa;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtFraccionamiento;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtColonia;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.TextBox txtApMat;
        private System.Windows.Forms.TextBox txtApPat;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtUsuarioContra;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNumSS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCertEstudios;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtActaDeNac;
        private System.Windows.Forms.ComboBox cbTipoUser;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgUsuarios;
        private System.Windows.Forms.Button btnRetroceder;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnBorrar;
    }
}