﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib_pdv_uth_v1.usuarios;
using System.Net.NetworkInformation;

namespace pdv_uth_v1
{
    public partial class FrmLogin : Form
    {
        private Timer ti;
        public static Usuario usuario;


        private void eventTimer(object ob, EventArgs e)
        {
            DateTime today = DateTime.Now;
            lblReloj.Text = today.ToString("hh:mm:ss tt");

        }
        public FrmLogin()
        {
            ti = new Timer();
            ti.Tick += new EventHandler(eventTimer);
            ti.Enabled = true;
            InitializeComponent();

        }
        //instancia de usuario
        public static Usuario us = new Usuario();

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            //cerrar la forma
            if (MessageBox.Show("¿Esta seguro que desea salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnIngresar_Click_1(object sender, EventArgs e)
        {
            loguearse();
          // FrmLogin.usuario = FrmLogin.usuario == null ? new Usuario() : FrmLogin.usuario;
          // FrmLogin.usuario = FrmLogin.usuario.loginUsuario(txtCorreo.Text, txtContraseña.Text);
          //
          // //login
          // if (FrmLogin.usuario.TipoUsuario == TipoUsuario.ADMINISTRADOR)
          // {
          //     FrmMenuPpal frmMenuPpal = new FrmMenuPpal();
          //     //escondemos el login
          //     this.Hide();
          //     //mostramos la forma de DIALOG para que est{e sobre todas enfrente
          //     frmMenuPpal.ShowDialog();
          //     //mostramos el login de nuevo login¿¿¿¿¿¿¿¿¿¿PARA QUE LO MOSTRAMOS DE NUEVO????????????????
          //     /*                this.Show();
          //     */
          // }
          // else if (FrmLogin.usuario.TipoUsuario == TipoUsuario.CAJERO)
          // {
          //     //abrimos el Caja
          //     FrmCaja frmcaja = new FrmCaja();
          //     //escondemos el l0gin
          //     this.Hide();
          //     //mostramos la forma de DIALOG para que est{e sobre todas enfrente
          //     frmcaja.ShowDialog();
          //     //mostramos el login de nuevo login ¿¿¿¿¿¿¿¿¿¿PARA QUE LO MOSTRAMOS DE NUEVO????????????????
          //     /*                this.Show();
          //     */
          // }
          // else
          // {
          //     //ERROR
          //     MessageBox.Show("Usuario no existe", "Error al ingresar", MessageBoxButtons.OK, MessageBoxIcon.Error);
          // }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro que desea salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }
        private void txtContraseña_TextChanged(object sender, EventArgs e)
        {
            // The password character is an asterisk.
            txtContraseña.PasswordChar = '*';
            // The control will allow no more than 14 characters.
            txtContraseña.MaxLength = 30;
        }


        private void loguearse()
        {  //login
            if (us.login(txtCorreo.Text, txtContraseña.Text) == TipoUsuario.ADMINISTRADOR)
            {
                FrmMenuPpal frmMenuPpal = new FrmMenuPpal();
                //escondemos el login
                this.Hide();
                //mostramos la forma de DIALOG para que est{e sobre todas enfrente
                frmMenuPpal.ShowDialog();
                //mostramos el login de nuevo login¿¿¿¿¿¿¿¿¿¿PARA QUE LO MOSTRAMOS DE NUEVO????????????????
                /*                this.Show();
                */
            }
            else if (us.login(txtCorreo.Text, txtContraseña.Text) == TipoUsuario.CAJERO)
            {
                //abrimos el Caja
                FrmCaja frmcaja = new FrmCaja();
                //escondemos el l0gin
                this.Hide();
                //mostramos la forma de DIALOG para que est{e sobre todas enfrente
                frmcaja.ShowDialog();
                //mostramos el login de nuevo login ¿¿¿¿¿¿¿¿¿¿PARA QUE LO MOSTRAMOS DE NUEVO????????????????
                /*                this.Show();
                */
            }
            else
            {
                //ERROR
                MessageBox.Show("Usuario no existe", "Error al ingresar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void txtContraseña_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                loguearse();
            }
        }

        private void txtCorreo_MouseClick(object sender, MouseEventArgs e)
        {
            txtCorreo.Text = "";
        }

        private void txtContraseña_Enter(object sender, EventArgs e)
        {
            txtContraseña.Text = "";
        }

        private void lblForgetPass_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           // LibNotificaciones.CorreoElectronico recuperarPassword = new LibNotificaciones.CorreoElectronico();
           //
           // if (txtCorreo.Text.Contains("@") && (txtCorreo.Text.Contains(".edu") || txtCorreo.Text.Contains(".com")) && txtCorreo.Text.Length >= 13)
           // {
           //     if (recuperarPassword.notificar(txtCorreo.Text, "Prueba", "Para restablecer tu contraseña de nuevo utiliza: 609-837. "))
           //     {
           //         MessageBox.Show("Tu contraseña se reenvio a tu correo...", "Recuperacion de contraseña", MessageBoxButtons.OK, MessageBoxIcon.Information);
           //     }
           //     else
           //     {
           //         MessageBox.Show("Escribe tu correo registrado para recuperar contraseña", "Restablecer contraseña. ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
           //     }
           // }
        }
    }
}
